--
-- Neovim settings file by Andreas Schneider <asn@cryptomilk.org>
-- Get the latest version from:
--     https://git.cryptomilk.org/users/asn/dotfiles.git/
--

-- Enable lua loader using the byte-compilation cache
if vim.fn.has('nvim-0.9') == 1 then
    vim.loader.enable()
end

uv = vim.loop or vim.uv

require('config/options')

local utils = require('utils')
if not (vim.fn.has('nvim-0.8') == 1) then
    utils.warn("This config requires neovim > 0.8")

    vim.o.lpl = true
    vim.o.termguicolors = true

    pcall(vim.cmd, [[colorscheme embark]])

    return
end

require('config/abbreviations')
require('config/autocmds')
require('config/keymaps')

if not utils.has_neovim_v_0_9() then
    -- :cscope* support has been removed in 0.9
    require('config/cscope')
end

if utils.am_i_root() then
    pcall(vim.cmd, [[colorscheme embark]])
    return
end

require('config/lazy').init()
require('config/ftdetect')
