local M = {}

function M._echo_multiline(msg)
    for _, s in ipairs(vim.fn.split(msg, '\n')) do
        vim.cmd("echom '" .. s:gsub("'", "''") .. "'")
    end
end

function M.info(msg)
    vim.cmd('echohl Directory')
    M._echo_multiline(msg)
    vim.cmd('echohl None')
end

function M.warn(msg)
    vim.cmd('echohl WarningMsg')
    M._echo_multiline(msg)
    vim.cmd('echohl None')
end

function M.err(msg)
    vim.cmd('echohl ErrorMsg')
    M._echo_multiline(msg)
    vim.cmd('echohl None')
end

-- Functions from
-- https://github.com/ibhagwan/nvim-lua/blob/main/lua/utils.lua

function M.shell_command(file)
    vim.fn.system(string.format("command -v '%s'", file))
    if vim.v.shell_error ~= 0 then
        return false
    else
        return true
    end
end

-- function M.toggle_colorcolumn()
-- local wininfo = vim.fn.getwininfo()
-- for _, win in pairs(wininfo) do
-- local ft = vim.api.nvim_buf_get_option(win['bufnr'], 'filetype')
-- -- print(win['winnr'], win['width'], ft)
-- if ft == nil or ft == '' or ft == 'TelescopePrompt' then return end
-- if not vim.g.colorcolumn then vim.g.colorcolumn = 81 end
-- if win['width'] < vim.g.colorcolumn then
-- vim.api.nvim_win_set_option(win['winid'], 'colorcolumn', '')
-- else
-- vim.api.nvim_win_set_option(win['winid'], 'colorcolumn', string.format(vim.g.colorcolumn))
-- end
-- end
-- end

function M.tablelength(T)
    local count = 0
    for _ in pairs(T) do
        count = count + 1
    end
    return count
end

-- QUICKFIX LISTS

-- open quickfix if not empty
function M.open_qf()
    local qf_name = 'quickfix'
    local qf_empty = function()
        return vim.tbl_isempty(vim.fn.getqflist())
    end
    if not qf_empty() then
        vim.cmd('copen')
        vim.cmd('wincmd J')
    else
        print(string.format('%s is empty.', qf_name))
    end
end

-- 'q': find the quickfix window
-- 'l': find all loclist windows
function M.find_qf(type)
    local wininfo = vim.fn.getwininfo()
    local win_tbl = {}
    for _, win in pairs(wininfo) do
        local found = false
        if type == 'l' and win['loclist'] == 1 then
            found = true
        end
        -- loclist window has 'quickfix' set, eliminate those
        if type == 'q' and win['quickfix'] == 1 and win['loclist'] == 0 then
            found = true
        end
        if found then
            table.insert(
                win_tbl,
                { winid = win['winid'], bufnr = win['bufnr'] }
            )
        end
    end
    return win_tbl
end

-- toggle quickfix/loclist on/off
-- type='*': qf toggle and send to bottom
-- type='l': loclist toggle (all windows)
function M.toggle_qf(type)
    local windows = M.find_qf(type)
    if M.tablelength(windows) > 0 then
        -- hide all visible windows
        for _, win in pairs(windows) do
            vim.api.nvim_win_hide(win.winid)
        end
    else
        -- no windows are visible, attempt to open
        if type == 'l' then
            M.open_loclist_all()
        else
            M.open_qf()
        end
    end
end

-- enum all non-qf windows and open
-- loclist on all windows where not empty
function M.open_loclist_all()
    local wininfo = vim.fn.getwininfo()
    local qf_name = 'loclist'
    local qf_empty = function(winnr)
        return vim.tbl_isempty(vim.fn.getloclist(winnr))
    end
    for _, win in pairs(wininfo) do
        if win['quickfix'] == 0 then
            if not qf_empty(win['winnr']) then
                -- switch active window before ':lopen'
                vim.api.nvim_set_current_win(win['winid'])
                vim.cmd('lopen')
            else
                print(string.format('%s is empty.', qf_name))
            end
        end
    end
end

function M.get_visual_selection(nl_literal)
    -- this will exit visual mode
    -- use 'gv' to reselect the text
    local _, csrow, cscol, cerow, cecol
    local mode = vim.fn.mode()
    if mode == 'v' or mode == 'V' or mode == '' then
        -- if we are in visual mode use the live position
        _, csrow, cscol, _ = unpack(vim.fn.getpos('.'))
        _, cerow, cecol, _ = unpack(vim.fn.getpos('v'))
        if mode == 'V' then
            -- visual line doesn't provide columns
            cscol, cecol = 0, 999
        end
        -- exit visual mode
        vim.api.nvim_feedkeys(
            vim.api.nvim_replace_termcodes('<Esc>', true, false, true),
            'n',
            true
        )
    else
        -- otherwise, use the last known visual position
        _, csrow, cscol, _ = unpack(vim.fn.getpos("'<"))
        _, cerow, cecol, _ = unpack(vim.fn.getpos("'>"))
    end
    -- swap vars if needed
    if cerow < csrow then
        csrow, cerow = cerow, csrow
    end
    if cecol < cscol then
        cscol, cecol = cecol, cscol
    end
    local lines = vim.fn.getline(csrow, cerow)
    -- local n = cerow-csrow+1
    local n = #lines
    if n <= 0 then
        return ''
    end

    lines[n] = string.sub(lines[n], 1, cecol)
    lines[1] = string.sub(lines[1], cscol)

    return table.concat(lines, nl_literal and '\\n' or '\n')
end

function M.git_commit_message_iab()
    local exec = vim.fn.system
    local git_user = exec('git config --get user.name'):gsub('\n', '')
    local git_email = exec('git config --get user.email'):gsub('\n', '')

    local signature = string.format('%s <%s>', git_user, git_email)

    vim.cmd('iab #S Signed-off-by: ' .. signature)
    vim.cmd('iab #R Reviewed-by: ' .. signature)
    vim.cmd('iab #O Signed-off-by:')
    vim.cmd('iab #V Reviewed-by:')
    vim.cmd('iab #P Pair-Programmed-With:')
    vim.cmd('iab ME ' .. signature)
    vim.cmd('iab AB Alexander<SPACE>Bokovoy<SPACE><ab@samba.org>')
    vim.cmd('iab ABARTLET Andrew<SPACE>Bartlet<SPACE><abartlet@samba.org>')
    vim.cmd('iab ARIS Aris<SPACE>Adamantiadis<SPACE><aris@0xbadc0de.be>')
    vim.cmd('iab ASN Andreas<SPACE>Schneider<SPACE><asn@samba.org>')
    vim.cmd('iab GD Guenther<SPACE>Deschner<SPACE><gd@samba.org>')
    vim.cmd('iab GLOCKYER Gary<SPACE>Lockyer<SPACE><gary@samba.org>')
    vim.cmd('iab JJELEN Jakub<SPACE>Jelen<SPACE><jjelen@redhat.com>')
    vim.cmd('iab JRA Jeremy<SPACE>Allison<SPACE><jra@samba.org>')
    vim.cmd('iab JSTEPHEN Justin<SPACE>Stephenson<SPACE><jstephen@redhat.com>')
    vim.cmd('iab METZE Stefan<SPACE>Metzmacher<SPACE><metze@samba.org>')
    vim.cmd('iab OBNOX Michael<SPACE>Adam<SPACE><obnox@samba.org>')
    vim.cmd('iab PFILIPEN Pavel<SPACE>Filipenský<SPACE><pfilipen@redhat.com>')
    vim.cmd('iab RH_ASN Andreas<SPACE>Schneider<SPACE><asn@redhat.com>')
    vim.cmd('iab SLOW Ralph<SPACE>Boehme<SPACE><slow@samba.org>')
    vim.cmd('iab URI Uri<SPACE>Simchoni<SPACE><uri@samba.org>')
    vim.cmd('iab VL Volker<SPACE>Lendecke<SPACE><vl@samba.org>')
end

function M.have_compiler()
    if
        vim.fn.executable('cc') == 1
        or vim.fn.executable('gcc') == 1
        or vim.fn.executable('clang') == 1
    then
        return true
    end

    return false
end

function M.feedkey(key)
    if key then
        vim.api.nvim_feedkeys(
            vim.api.nvim_replace_termcodes(key, true, true, true),
            'n',
            true
        )
    end
end

function M.am_i_root()
    return (uv.getuid() == 0)
end

function M.has_neovim_v_0_8()
    return (vim.fn.has('nvim-0.8') == 1)
end

function M.has_neovim_v_0_9()
    return (vim.fn.has('nvim-0.9') == 1)
end

function M.is_dev(path)
    local devdir = '~/workspace/prj/oss/'
    return uv.fs_stat(string.format('%s/%s', vim.fn.expand(devdir), path))
end

--- Executes a command using sudo
---
--- @param args string[]
--- @param output boolean
--- @param onexit function
M.sudo_exec = function(args, output, onexit)
    -- Thanks to haolian9 from the Neovim Matrix channel for the improvements!
    onexit = onexit or function(_, _) end
    local state = {
        term_width = nil,
        term_height = nil,
        bufnr = nil,
        term = nil,
        job = nil,
    }

    local cols, lines = vim.o.columns, vim.o.lines

    state.term_width = math.min(cols, math.max(math.floor(cols * 0.8), 80))
    state.term_height = math.min(lines, math.max(math.floor(lines * 0.3), 5))

    state.bufnr = vim.api.nvim_create_buf(false, true)

    ---@param _output string[]
    ---@param gold string
    local output_contains = function(_output, gold)
        for _, line in pairs(_output) do
            if string.find(line, gold, 1, true) then
                return true
            end
        end
        return false
    end

    local show_prompt = function(winbar)
        if not (state.win_id and vim.api.nvim_win_is_valid(state.win_id)) then
            local width = math.min(
                vim.o.columns,
                math.max(math.floor(vim.o.columns * 0.8), 50)
            )
            local height = math.min(
                vim.o.lines,
                math.max(math.floor(vim.o.lines * 0.5), 20)
            )
            local x = vim.o.lines - height - 20
            local y = math.floor((vim.o.columns - width) / 2)

            state.win_id = vim.api.nvim_open_win(state.bufnr, true, {
                relative = 'editor',
                style = 'minimal',
                border = 'rounded',
                row = y,
                col = x,
                width = width,
                height = height,
            })
        else
            vim.api.nvim_set_current_win(state.win_id)
        end
        vim.wo[state.win_id].winbar = winbar
        vim.cmd.startinsert()
    end

    local show_float = function(winbar)
        if not (state.win_id and vim.api.nvim_win_is_valid(state.win_id)) then
            local width = state.term_width + 2
            local height = state.term_height + 2
            local x = math.floor((cols - width) / 2)
            local y = lines - height

            state.win_id = vim.api.nvim_open_win(state.bufnr, true, {
                relative = 'editor',
                style = 'minimal',
                border = 'rounded',
                row = y,
                col = x,
                width = width,
                height = height,
            })
        else
            vim.api.nvim_set_current_win(state.win_id)
        end
        vim.wo[state.win_id].winbar = winbar
    end

    state.term = vim.api.nvim_open_term(state.bufnr, {
        on_input = function(_, _, _, data)
            vim.fn.chansend(state.job, data)
            -- todo: can be called on_exit for better UX
            if data == '\r' then
                vim.schedule(function()
                    vim.api.nvim_win_close(state.win_id, false)
                    state.win_id = nil
                end)
            end
        end,
    })

    local sudo_cmd = { 'sudo' }
    for _, v in ipairs(args) do
        table.insert(sudo_cmd, v)
    end

    state.job = vim.fn.jobstart(sudo_cmd, {
        pty = true,
        width = state.term_width,
        height = state.term_height,
        stdin = 'pipe',
        stdout_buffered = false,
        stderr_buffered = false,
        env = { ['LANG'] = 'C' },
        on_exit = function(_, exit_code, event)
            vim.fn.chanclose(state.term)
            vim.api.nvim_buf_delete(state.bufnr, { force = false })
            onexit(exit_code, event)
        end,
        on_stdout = function(_, data, _)
            if data == nil then
                return
            end
            vim.fn.chansend(state.term, data)
            if output_contains(data, '[sudo] password') then
                show_prompt(
                    string.format('%s [stdout]', table.concat(sudo_cmd, ' '))
                )
            elseif output_contains(data, 'Please touch the device') then
                show_float(
                    string.format('%s [stdout]', table.concat(sudo_cmd, ' '))
                )
            elseif output then
                if #data[1] > 0 then
                    local pretty = string.gsub(data[1], '\r', '')

                    print(pretty)
                end
            end
        end,
        on_stderr = function(_, data, _)
            if data == nil then
                return
            end
            if output_contains(data, '[sudo] password') then
                show_prompt(
                    string.format('%s [stderr]', table.concat(sudo_cmd, ' '))
                )
            elseif output_contains(data, 'Please touch the device') then
                show_float(
                    string.format('%s [stderr]', table.concat(sudo_cmd, ' '))
                )
            elseif output then
                if #data[1] > 0 then
                    local pretty = string.gsub(data[1], '\r', '')

                    print(pretty)
                end
            end
            vim.fn.chansend(state.term, data)
        end,
    })
end

local nvim_exec2 = function(cmd, opts)
    if opts.output == nil then
        opts.output = false
    end
    if vim.fn.has('nvim-0.9') == 1 then
        return vim.api.nvim_exec2(cmd, opts)
    end
    return vim.api.nvim_exec(cmd, opts.output)
end

--- Write a file using sudo
---
--- @param tmpfile string
--- @param filepath string
M.sudo_write = function(tmpfile, filepath)
    tmpfile = tmpfile or vim.fn.tempname()
    filepath = filepath or vim.fn.expand('%')

    if not filepath or #filepath == 0 then
        M.err('E32: No file name')
        return
    end

    local cmd = {
        'dd',
        'if=' .. tmpfile,
        'of=' .. filepath,
        'bs=16384',
    }

    nvim_exec2(string.format('write! %s', tmpfile), {})

    M.sudo_exec(cmd, false, function(code, _)
        vim.schedule(function()
            if code == 0 then
                M.info(string.format('\r\n"%s" written', filepath))
                nvim_exec2('edit!', {})
            else
                M.err(string.format('Failed to write %s', filepath))
            end
            vim.fn.delete(tmpfile)
        end)
    end)
end

function M.is_git_dir()
    if
        vim.fn.system("git rev-parse --is-inside-work-tree | tr -d '\n'")
        == 'true'
    then
        return true
    else
        return false
    end
end

return M
