if not pcall(require, 'lspconfig') then
    return
end

require('lsp.diagnostics')

require('lsp.icons').setup()

local lsp_on_attach = require('lsp.on_attach').on_attach

-----------------------------------------------------------
-- LSP SERVER SETUP
-----------------------------------------------------------

local lsp_servers = {}
local lsp_config = require('lspconfig')

if vim.fn.executable('clangd') == 1 then
    table.insert(lsp_servers, 'clangd')
end

if vim.fn.executable('jedi-language-server') == 1 then
    table.insert(lsp_servers, 'jedi_language_server')
end

if vim.fn.executable('pylsp') == 1 then
    table.insert(lsp_servers, 'pylsp')
end

if vim.fn.executable('pyright-langserver') == 1 then
    table.insert(lsp_servers, 'pyright')
end

if vim.fn.executable('bash-language-server') == 1 then
    table.insert(lsp_servers, 'bashls')
end

if vim.fn.executable('ruby-lsp') == 1 then
    table.insert(lsp_servers, 'ruby_lsp')
end

if vim.fn.executable('neocmakelsp') == 1 then
    table.insert(lsp_servers, 'neocmake')
end

local lsp_capabilities = vim.lsp.protocol.make_client_capabilities()

-- nvim-cmp supports LSP's capabilities, advertise it to LSP servers.
if pcall(require, 'cmp') and pcall(require, 'cmp_nvim_lsp') then
    local cmp_lsp_cap = require('cmp_nvim_lsp').default_capabilities()
    -- Merge lsp_capabilities and cmp_lsp_cap
    lsp_capabilities =
        vim.tbl_deep_extend('force', lsp_capabilities, cmp_lsp_cap or {})
end

-- Enable inotify lsp watchfunc backend - :help inotify-limitations
if vim.fn.has('nvim-0.11') == 1 and vim.fn.executable('inotifywait') == 1 then
    lsp_capabilities.workspace.didChangeWatchedFiles.dynamicRegistration = true
end

local lsp_flags = {
    debounce_text_changes = 200,
}

-- Configure lua language server for neovim development
if vim.fn.executable('lua-language-server') == 1 then
    lsp_config.lua_ls.setup({
        cmd = { 'lua-language-server' },
        on_attach = lsp_on_attach,
        capabilities = lsp_capabilities,
        flags = lsp_flags,
        settings = {
            Lua = {
                runtime = {
                    -- LuaJIT in the case of Neovim
                    version = 'LuaJIT',
                    path = vim.split(package.path, ';'),
                },
                diagnostics = {
                    -- Get the language server to recognize the `vim` global
                    globals = { 'vim' },
                },
                workspace = {
                    -- Make the server aware of Neovim runtime files
                    library = {
                        [vim.fn.expand('$VIMRUNTIME/lua')] = true,
                        [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
                    },
                },
                telemetry = {
                    enable = false,
                },
            },
        },
    })
end

-- NOTE: Diagnostics are provided by running cargo check and rust-analyzer
-- doesn't have any way of running cargo check on unsaved files!
--
-- rust-analyzer does have some diagnostics it computes itself, which do show
-- up immediately (some of them are experimental and disabled by default). The
-- plan is to provide all of them in this way, and the cargo check approach is
-- just a stopgap until that's the case. It's still going to take some time
-- though.
--
-- https://rust-analyzer.github.io/book/configuration.html
local rust_settings = {
    ['rust-analyzer'] = {
        check = {
            command = 'clippy',
            extraArgs = { '--', '-W', 'clippy::pedantic' },
        },
        diagnostics = {
            experimental = {
                enable = true,
            },
            styleLints = {
                enable = true,
            },
            disabled = {
                'unresolved-proc-macro',
            },
        },
    },
}

if vim.fn.executable('rustup') == 1 then
    lsp_config.rust_analyzer.setup({
        on_attach = lsp_on_attach,
        capabilities = lsp_capabilities,
        flags = lsp_flags,
        cmd = { 'rustup', 'run', 'stable', 'rust-analyzer' },
        settings = rust_settings,
    })
elseif vim.fn.executable('rust-analyzer') == 1 then
    lsp_config.rust_analyzer.setup({
        on_attach = lsp_on_attach,
        capabilities = lsp_capabilities,
        flags = lsp_flags,
        settings = rust_settings,
    })
end

if vim.fn.executable('gopls') == 1 then
    lsp_config.gopls.setup({
        on_attach = lsp_on_attach,
        capabilities = lsp_capabilities,
        flags = lsp_flags,
        -- Server-specific settings
        settings = {
            ['gopls'] = {
                gofumpt = (vim.fn.executable('gofumpt') == 1),
            },
        },
    })
end

for _, lsp in ipairs(lsp_servers) do
    lsp_config[lsp].setup({
        on_attach = lsp_on_attach,
        capabilities = lsp_capabilities,
        flags = lsp_flags,
    })
end
