-- https://github.com/neovim/nvim-lspconfig/wiki/UI-Customization
local signs = {
    {
        name = 'DiagnosticSignHint',
        text = '󰌵',
    },
    {
        name = 'DiagnosticSignInfo',
        -- text = '',
        text = '',
    },
    {
        name = 'DiagnosticSignWarn',
        -- text = '',
        text = '',
    },
    {
        name = 'DiagnosticSignError',
        -- text = ''
        text = '',
    },
}

local sign_opts -- changes depending on nvim version

if vim.fn.has('nvim-0.10') == 1 then
    sign_opts = { text = {} }

    for _, sign in ipairs(signs) do
        table.insert(sign_opts.text, sign.text)
    end
else
    sign_opts = true
    for i = 1, #signs do
        signs[i].texthl = signs[i].name
    end

    vim.fn.sign_define(signs)
end

-- Diagnostics config
vim.diagnostic.config({
    underline = true,
    update_in_insert = false,
    virtual_text = {
        prefix = '●',
        spacing = 4,
        source = 'if_many',
        severity = {
            min = vim.diagnostic.severity.HINT,
        },
    },
    signs = sign_opts,
    severity_sort = true,
    float = {
        show_header = false,
        source = 'always',
        border = 'rounded',
    },
})

return {
    toggle = function()
        if not vim.g.diag_is_hidden then
            require('utils').info('Diagnostic virtual text is now hidden.')
            vim.diagnostic.hide()
        else
            require('utils').info('Diagnostic virtual text is now visible.')
            vim.diagnostic.show()
        end
        vim.g.diag_is_hidden = not vim.g.diag_is_hidden
    end,
}
