M = {}

-- This requires https://nerdfont.com/
local lsp_symbols = {
    Text = ' (text)',
    Method = ' (method)',
    Function = ' (func)',
    Constructor = ' (ctor)',
    Field = ' (field)',
    Variable = ' (var)',
    Class = ' (class)',
    Interface = 'ﰮ (interface)',
    Module = ' (module)',
    Property = ' (property)',
    Unit = 'ﰩ (unit)',
    Value = ' (value)',
    Enum = 'ﬧ (enum)',
    Keyword = ' (keyword)',
    Snippet = '﬌ (snippet)',
    Color = ' (color)',
    File = ' (file)',
    Reference = ' (ref)',
    Folder = ' (folder)',
    EnumMember = ' (enum member)',
    Constant = 'ﱃ (const)',
    Struct = ' (struct)',
    Event = ' (event)',
    Operator = '璉(operator)',
    TypeParameter = ' (type param)',
}

function M.setup()
    for k, s in pairs(lsp_symbols) do
        require('vim.lsp.protocol').CompletionItemKind[k] = s
    end
end

function M.cmp_format(opts)
    if opts == nil then
        opts = {}
    end

    return function(entry, vim_item)
        if opts.menu ~= nil then
            vim_item.menu = opts.menu[entry.source.name]
        end
        vim_item.kind = lsp_symbols[vim_item.kind]

        return vim_item
    end
end

return M
