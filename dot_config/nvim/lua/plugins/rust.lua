local M = {
    'vxpm/ferris.nvim',
    ft = 'rust',
}

function M.config()
    require("ferris").setup()
end

return M
