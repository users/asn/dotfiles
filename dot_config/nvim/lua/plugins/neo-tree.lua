local M = {
    'nvim-neo-tree/neo-tree.nvim',
    dependencies = {
        'MunifTanjim/nui.nvim',
    },
    -- cmd = 'Neotree'
    lazy = false,
    version = '*',
}

function M.init()
    local keymap = vim.keymap.set

    -- Remove legacy commands
    vim.g.neo_tree_remove_legacy_commands = 1

    keymap(
        'n',
        '<Leader>tt',
        '<cmd>Neotree reveal left<CR>',
        { desc = 'open neo-tree (left)' }
    )
    keymap(
        'n',
        '<Leader>tf',
        '<cmd>Neotree reveal float<CR>',
        { desc = 'open neo-tree (float)' }
    )
end

function M.config()
    require('neo-tree').setup({
        -- log_level = "trace",
        -- log_to_file = true,

        close_if_last_window = false, -- Close Neo-tree if it is the last window left in the tab
        popup_border_style = 'rounded',
        enable_git_status = false,
        enable_diagnostics = false,
        sort_case_insensitive = false, -- used when sorting files and directories in the tree
        resize_timer_interval = -1,
        default_component_configs = {
            indent = {
                indent_size = 2,
                padding = 1, -- extra padding on left hand side
            },
        },
        window = {
            position = 'left',
            width = 36,
        },
        filesystem = {
            async_directory_scan = 'always',
            use_libuv_file_watcher = true,
            bind_to_cwd = true,
            filtered_items = {
                hide_dotfiles = false,
                hide_gitignored = false,
            },
        },
    })
end

return M
