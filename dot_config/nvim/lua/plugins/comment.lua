if vim.fn.has('nvim-0.10') == 1 then
    -- See :help commenting
    return {}
end

local M = {
    'numToStr/Comment.nvim',
    event = 'VeryLazy',
}

function M.config()
    local A = vim.api
    local C = require('Comment.config')
    local U = require('Comment.utils')
    local ft = require('Comment.ft')

    ft.set('c', { '/*%s*/', '#if 0%s#endif' })
    ft.set('cpp', { '/*%s*/', '#if 0%s#endif' })

    function string.rfind(s, pattern, init, plain)
        assert(pattern ~= '')

        local _plain = plain or false
        local _init = init or -1
        if _init < 0 then
            _init = _init + #s + 1
        end

        local start = _init
        local first, last

        while true do
            local _first, _last = string.find(s, pattern, start, _plain)
            if not _first or _first > _last then
                break
            end
            start = _last + 1
            first, last = _first, _last
        end

        return first, last
    end

    local post_hook = function(ctx)
        if
            vim.bo.filetype ~= 'c'
            and vim.bo.filetype ~= 'cpp'
            and vim.bo.filetype ~= 'lua'
        then
            return
        end

        if ctx.range.srow == -1 then
            return
        end

        if ctx.ctype == 1 then
            return
        end

        local cfg = C:get()
        local cstr = ft.get(vim.bo.filetype, ctx.ctype)
        local lcs, rcs = U.unwrap_cstr(cstr)
        local padding = U.get_pad(cfg.padding)
        local lines =
            A.nvim_buf_get_lines(0, ctx.range.srow - 1, ctx.range.erow, false)

        if ctx.cmode == 1 then
            -- comment
            local str = lines[1]
            local i, j = string.find(str, lcs .. padding, 1, true)
            lines[1] = string.sub(str, i, j - #padding)
            table.insert(
                lines,
                2,
                string.sub(str, 0, i - 1) .. string.sub(str, j + #padding, #str)
            )

            str = lines[#lines]
            i, j = string.rfind(str, rcs, 1, true)
            lines[#lines] = string.sub(str, 0, i - #padding - 1)
            table.insert(lines, #lines + 1, string.sub(str, i, j))
        elseif ctx.cmode == 2 then
            -- uncomment
            if #lines[1] == 0 and #lines[#lines] == 0 then
                table.remove(lines, 1)
                table.remove(lines, #lines)
            end
        end

        vim.api.nvim_buf_set_lines(
            0,
            ctx.range.srow - 1,
            ctx.range.erow,
            false,
            lines
        )
    end

    require('Comment').setup({
        post_hook = post_hook,
    })
end

return M
