local M = {
    'hiphish/rainbow-delimiters.nvim',
    event = 'FileType',
}

---@diagnostic disable-next-line: unused-local
local rainbow_test = { { { { { { {} } } } } } }

return M
