local M = {
    'folke/which-key.nvim',
    event = 'BufEnter',
    version = '*', -- use stable branch
}

function M.config()
    local wk = require('which-key')
    local keymap = vim.keymap.set

    keymap('n', '<Leader>?', "<Esc>:WhichKey '' n<CR>", { silent = true })
    keymap('v', '<Leader>?', "<Esc>:WhichKey '' v<CR>", { silent = true })

    -- https://github.com/folke/which-key.nvim#colors
    if vim.g.colors_name == 'gruvbox' then
        vim.api.nvim_set_hl(0, 'WhichKey', { link = 'htmlH1' })
        vim.api.nvim_set_hl(0, 'WhichKeySeperator', { link = 'String' })
        vim.api.nvim_set_hl(0, 'WhichKeyGroup', { link = 'Keyword' })
        vim.api.nvim_set_hl(0, 'WhichKeyDesc', { link = 'Include' })
        vim.api.nvim_set_hl(0, 'WhichKeyFloat', { link = 'CursorLine' })
        vim.api.nvim_set_hl(0, 'WhichKeyValue', { link = 'Comment' })
    end

    wk.setup({
        plugins = {
            marks = true, -- shows a list of your marks on ' and `
            registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
            -- the presets plugin, adds help for a bunch of default keybindings in Neovim
            -- No actual key bindings are created
            spelling = {
                enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
                suggestions = 20, -- how many suggestions should be shown in the list?
            },
            presets = {
                operators = true, -- adds help for operators like d, y, ... and registers them for motion / text object completion
                motions = true, -- adds help for motions
                text_objects = true, -- help for text objects triggered after entering an operator
                windows = true, -- default bindings on <c-w>
                nav = true, -- misc bindings to work with windows
                z = true, -- bindings for folds, spelling and others prefixed with z
                g = true, -- bindings for prefixed with g
            },
        },
        icons = {
            breadcrumb = '»', -- symbol used in the command line area that shows your active key combo
            separator = '➜', -- symbol used between a key and it's label
            group = ' ', -- symbol prepended to a group
        },
        win = {
            height = { min = 10, max = 25 },
            border = 'rounded', -- none, single, double, shadow
            padding = { 1, 1 }, -- extra window padding [top/bottom, right/left]
        },
        layout = {
            width = { min = 20, max = 50 }, -- min and max width of the columns
            spacing = 3, -- spacing between columns
        },
    })

    wk.add({
        { '.', desc = 'Repeat last edit', nowait = false },
        { '<C-L>', desc = 'Split window', nowait = false },
        { '<C-R>', desc = 'Redo', nowait = false },
        { '<C-S>', desc = 'Save', nowait = false },
        { '<down>', desc = 'Go down visual line', nowait = false },
        { '<leader>?', desc = 'Which key help', nowait = false },
        { '<leader>c', group = 'code', nowait = false },
        { '<leader>d', group = 'dap', nowait = false },
        { '<leader>f', group = 'find', nowait = false },
        { '<leader>g', group = 'git', nowait = false },
        { '<leader>h', group = 'gitsigns', nowait = false },
        { '<leader>l', group = 'lsp', nowait = false },
        { '<leader>t', group = 'tab/tree', nowait = false },
        { '<leader>tN', desc = 'open a new tab', nowait = false },
        { '<leader>tc', desc = 'close current tab', nowait = false },
        { '<leader>tf', desc = 'jump to first tab', nowait = false },
        { '<leader>tl', desc = 'jump to last tab', nowait = false },
        { '<leader>tn', desc = 'jump to next tab', nowait = false },
        {
            '<leader>to',
            desc = 'jump to first tab and close all others',
            nowait = false,
        },
        { '<leader>tp', desc = 'jump to previous tab', nowait = false },
        { '<leader>x', group = 'trouble', nowait = false },
        { '<up>', desc = 'Go up visual line', nowait = false },
        { 'U', desc = 'Undo line', nowait = false },
        { '[-', desc = 'goto older error list', nowait = false },
        { '[D', desc = 'diagnostics prev (nowrap)', nowait = false },
        { '[c', desc = 'diff (change) prev', nowait = false },
        { '[d', desc = 'diagnostics prev (wrap)', nowait = false },
        { ']+', desc = 'goto newer error list', nowait = false },
        { ']D', desc = 'diagnostics next (nowrap)', nowait = false },
        { ']c', desc = 'diff (change) next', nowait = false },
        { ']d', desc = 'diagnostics next (wrap)', nowait = false },
        { 'g', group = 'Goto (LSP)', nowait = false },
        {
            'g#',
            'g#:nohl<cr>',
            desc = 'goto prev word under cursor',
            nowait = false,
        },
        { 'g$', desc = 'goto line end (nowrap)', nowait = false },
        { 'g%', desc = 'goto previous matching group', nowait = false },
        {
            'g*',
            'g*:nohl<cr>',
            desc = 'goto next word under cursor',
            nowait = false,
        },
        { 'g0', desc = 'goto line start (nowrap)', nowait = false },
        { 'g8', desc = 'print hex value under cursor', nowait = false },
        { 'g<', desc = 'display last !command output', nowait = false },
        { 'g<C-G>', desc = 'show current cursor pos info', nowait = false },
        {
            'g<C-V>',
            desc = 'visually select last yanked/pasted text',
            nowait = false,
        },
        { 'gE', desc = 'Previous end of WORD', nowait = false },
        { 'gF', desc = 'Goto file:line under cursor', nowait = false },
        { 'gI', desc = 'insert at first column', nowait = false },
        { 'gM', desc = 'goto middle of text line', nowait = false },
        { 'gT', desc = 'goto prev tab', nowait = false },
        { 'g_', desc = 'goto last non-EOL char', nowait = false },
        { 'gt', desc = 'goto next tab', nowait = false },
        { 'u', desc = 'Undo', nowait = false },
    })
end

return M
