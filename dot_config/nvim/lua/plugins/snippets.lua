local M = {
    'dcampos/nvim-snippy',
    dependencies = {
        'honza/vim-snippets',
    },
    event = { 'BufReadPost', 'InsertEnter' },
}

function M.config()
    local snippy = require('snippy')
    local keymap = vim.keymap.set
    local feedkey = require('utils').feedkey

    keymap('i', '<C-j>', function(fallback)
        if snippy.can_expand_or_advance() then
            snippy.expand_or_advance()
        else
            feedkey(fallback)
        end
    end, { desc = 'Expand or advance snippet' })
    keymap('s', '<C-j>', function(fallback)
        if snippy.can_expand_or_advance() then
            snippy.expand_or_advance()
        else
            feedkey(fallback)
        end
    end, { desc = 'Expand or advance snippet' })

    keymap('i', '<C-k>', function(fallback)
        if snippy.can_jump(-1) then
            snippy.previous()
        else
            feedkey(fallback)
        end
    end, { desc = 'Jump to previous snippet part' })
    keymap('s', '<C-k>', function(fallback)
        if snippy.can_jump(-1) then
            snippy.previous()
        else
            feedkey(fallback)
        end
    end, { desc = 'Jump to previous snippet part' })
end

return M
