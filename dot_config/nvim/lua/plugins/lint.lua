local M = {
    'mfussenegger/nvim-lint',
    event = 'BufReadPre',
    -- dev = require('utils').is_dev('nvim-lint'),
}

function M.config()
    local lint = require('lint')

    lint.linters_by_ft = {
        dockerfile = { 'hadolint' },
        gitcommit = { 'codespell', 'vale' },
        json = { 'jq' },
        markdown = { 'vale' },
        norg = { 'vale' },
        php = { 'php' },
        rst = { 'vale' },
        ruby = { 'ruby' },
        spec = { 'rpmspec' },
        text = { 'vale' },
        yaml = { 'yamllint' },
        ['yaml.ansible'] = { 'ansiblelint' },
    }

    local augroup = vim.api.nvim_create_augroup('Lint', { clear = true })
    vim.api.nvim_create_autocmd({
        'BufWritePost',
        'FocusGained',
        'InsertLeave',
        'TextChanged',
    }, {
        group = augroup,
        callback = function(args)
            vim.api.nvim_buf_call(args.buf, function()
                lint.try_lint(nil, { ignore_errors = true })
            end)
        end,
    })
    lint.try_lint(nil, { ignore_errors = true })
end

return M
