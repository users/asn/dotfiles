local M = {
    'j-hui/fidget.nvim',
    dependencies = {
        'neovim/nvim-lspconfig',
    },
    event = 'LspAttach',
}

function M.config()
    require('fidget').setup({
        progress = {
            display = {
                -- Icon shown when LSP progress tasks are in progress
                progress_icon = { pattern = 'dots_pulse', period = 1 },
                -- Highlight group for in-progress LSP tasks
                progress_style = 'WarningMsg',
            },
        },
        notification = {
            window = {
                align = "top",
            },
        },
    })
end

return M
