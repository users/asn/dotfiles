local M = {
    'lewis6991/gitsigns.nvim',
    dependencies = {
        'nvim-lua/plenary.nvim',
    },
    -- VeryLazy hides splash screen
    event = 'BufReadPost',
    version = '*', -- use stable branch
}

function M.config()
    require('gitsigns').setup({
        signs = {
            add = { text = '▍' },
            change = { text = '▍' },
            changedelete = { text = '█' },
            delete = { text = '▁' },
            topdelete = { text = '▔' },
        },

        watch_gitdir = { interval = 1000 },
        preview_config = { border = 'rounded' },
        diff_opts = { internal = true },

        current_line_blame_formatter = '<author_time:%Y>, <author> - <summary>',

        sign_priority = 4, -- Lower priorirty means diag signs supercede

        on_attach = function(bufnr)
            local gs = package.loaded.gitsigns

            local function keymap(mode, l, r, opts)
                opts = opts or {}
                opts.buffer = bufnr
                vim.keymap.set(mode, l, r, opts)
            end

            keymap('n', ']c', function()
                if vim.wo.diff then
                    vim.cmd.normal({ ']c', bang = true })
                else
                    gs.nav_hunk('next')
                end
            end, { desc = 'Next hunk' })

            keymap('n', '[c', function()
                if vim.wo.diff then
                    vim.cmd.normal({ '[c', bang = true })
                else
                    gs.nav_hunk('prev')
                end
            end, { desc = 'Previous hunk' })

            -- Actions
            keymap(
                { 'n', 'v' },
                '<leader>hs',
                gs.stage_hunk,
                { desc = 'stage hunk' }
            )
            keymap(
                { 'n', 'v' },
                '<leader>hr',
                gs.reset_hunk,
                { desc = 'reset hunk (git)' }
            )
            keymap(
                'n',
                '<leader>hS',
                gs.stage_buffer,
                { desc = 'stage buffer (git)' }
            )
            keymap(
                'n',
                '<leader>hu',
                gs.undo_stage_hunk,
                { desc = 'unstage hunk (git)' }
            )
            keymap(
                'n',
                '<leader>hR',
                gs.reset_buffer,
                { desc = 'reset hunk (git)' }
            )
            keymap(
                'n',
                '<leader>hp',
                gs.preview_hunk,
                { desc = 'preview hunk (git)' }
            )
            keymap(
                'n',
                '<leader>hb',
                function()
                    gs.blame_line({full = true})
                end,
                { desc = 'blame/annotate (git)' }
            )
            keymap(
                'n',
                '<leader>tb',
                gs.toggle_current_line_blame,
                { desc = 'toogle line blame (git)' }
            )
            keymap(
                'n',
                '<leader>hd',
                gs.diffthis,
                { desc = 'diff against last commit (git)' }
            )
            keymap(
                'n',
                '<leader>hD',
                function ()
                    gs.diffthis('~1')
                end,
                { desc = 'diff against last commit (git)' }
            )
            keymap(
                'n',
                '<leader>td',
                gs.toggle_word_diff,
                { desc = 'toggle word diff (git)' }
            )
            keymap(
                'n',
                '<leader>tx',
                gs.toggle_deleted,
                { desc = 'toggle deleted (git)' }
            )

            -- Text object
            keymap(
                { 'o', 'x' },
                'ih',
                ':<C-U>Gitsigns select_hunk<CR>',
                { desc = 'select hunk (git)' }
            )
        end,
    })
end

return M
