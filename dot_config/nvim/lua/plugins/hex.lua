local M = {
    'RaafatTurki/hex.nvim',
    enabled = require('utils').shell_command('xxd'),
    event = 'VeryLazy',
}

function M.init()
    local keymap = vim.keymap.set

    keymap('n', '<leader>H', function()
        require('hex').toggle()
    end, { desc = 'Toggle hex editor' })
end

function M.config()
    require('hex').setup()
end

return M
