local M = {
    'Saecki/crates.nvim',
    event = { "BufRead Cargo.toml" },
    version = '*',
}

function M.config()
    require('crates').setup()
end

return M
