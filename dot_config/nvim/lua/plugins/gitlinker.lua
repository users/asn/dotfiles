local M = {
    'linrongbin16/gitlinker.nvim',
    -- VeryLazy hides splash screen
    event = 'BufReadPost',
    version = '*', -- use stable branch
}

function M.config()
    require('gitlinker').setup({
        router = {
            browse = {
                ['^git.samba.org'] = 'https://git.samba.org/'
                    .. '?p={_A.REPO}.git'
                    .. ";{(string.len(_A.FILE) >= 3 and _A.FILE:sub(#_A.FILE-2) == '.md') and 'a=blob_plain=1' or 'a=blob'}" -- '?blob_plain'
                    .. ';f={_A.FILE}'
                    -- .. ';h={_A.REV}'
                    .. ';hb=HEAD'
                    .. '#l{_A.LSTART}'
            },
        },
    })

    vim.keymap.set({ 'n', 'v' }, '<leader>hl', '<cmd>GitLink<cr>', {
        silent = true,
        noremap = true,
        desc = 'Copy git permlink to clipboard',
    })
end

return M
