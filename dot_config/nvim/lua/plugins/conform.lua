local M = {
    'stevearc/conform.nvim',
    event = 'BufReadPre'
}

function M.config()
    local conform = require('conform')

    conform.setup({
        -- Conform checks if the commands are executable
        formatters_by_ft = {
            lua = { 'stylua' },
            -- Use a sub-list to run only the first available formatter
            go = { { 'gofumpt', 'gofmt' } },
            javascript = { 'prettier' },
            rust = { 'rustfmt' },
            sh = { 'shfmt' },
            yaml = { 'yamlfmt' },
        },
        formatters = {
            shfmt = vim.tbl_extend('force', require('conform.formatters.shfmt'), {
                -- You can set the args dynamically by providing a callback
                args = function(_, ctx)
                    if string.find(ctx.dirname, 'samba', 1, true) then
                        return {
                            '--posix',
                            '--indent',
                            '0',
                            '-filename',
                            '$FILENAME',
                        }
                    else
                        return { '--indent', '4', '-filename', '$FILENAME' }
                    end
                end,
            }),
        },
    })

    -- The conform formatexpr should fall back to LSP when no formatters are
    -- available, and fall back to the internal default if no LSP clients are
    -- available. So it should be safe to set it globally.
    vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
end

return M
