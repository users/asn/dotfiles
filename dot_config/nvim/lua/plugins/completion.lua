local M = {
    'hrsh7th/nvim-cmp',
    dependencies = {
        'hrsh7th/cmp-nvim-lua',
        'hrsh7th/cmp-nvim-lsp',
        'hrsh7th/cmp-path',
        'hrsh7th/cmp-buffer',
        'dcampos/cmp-snippy',
        'ray-x/lsp_signature.nvim',
    },
    event = 'InsertEnter',
}

function M.config()
    local cmp = require('cmp')
    local snippy = require('snippy')
    local lsp_sig = require('lsp_signature')

    local t = function(str)
        return vim.api.nvim_replace_termcodes(str, true, true, true)
    end

    local check_back_space = function()
        local col = vim.fn.col('.') - 1
        return col == 0 or vim.fn.getline('.'):sub(col, col):match('%s')
    end

    cmp.setup({
        completion = {
            keyword_length = 2,
        },

        snippet = {
            expand = function(args)
                snippy.expand_snippet(args.body)
            end,
        },

        sources = {
            { name = 'path' },
            { name = 'snippy' },
            { name = 'buffer', keyword_length = 5 },
        },

        experimental = {
            ghost_text = true,
        },

        mapping = {
            ['<Tab>'] = function(fallback)
                if cmp.visible() then
                    cmp.select_next_item()
                elseif snippy.can_expand_or_advance() then
                    vim.fn.feedkeys(t('<Plug>(snippy-expand-or-next)'), '')
                elseif not check_back_space() then
                    cmp.complete()
                else
                    vim.fn.feedkeys(t('<Tab>'), 'n')
                end
            end,
            ['<C-p>'] = cmp.mapping.select_prev_item(),
            ['<C-n>'] = cmp.mapping.select_next_item(),
            ['<C-d>'] = cmp.mapping.scroll_docs(-4),
            ['<C-f>'] = cmp.mapping.scroll_docs(4),
            ['<C-Space>'] = cmp.mapping.complete(),
            ['<C-e>'] = cmp.mapping.close(),
            ['<CR>'] = cmp.mapping.confirm({
                behavior = cmp.ConfirmBehavior.Replace,
                select = true,
            }),
        },
    })

    lsp_sig.setup({
        bind = true,
        floating_window = false, -- floating window is too big
        hint_enable = true,
        hint_prefix = ' ',
        hint_scheme = 'String',
        toggle_key = '<c-h>',
        handler_opts = {
            border = 'rounded',
        },
    })
end

return M
