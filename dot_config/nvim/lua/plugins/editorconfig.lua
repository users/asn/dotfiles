local M = {
    enabled = not require('utils').has_neovim_v_0_9(),
    'gpanders/editorconfig.nvim',
    event = 'BufReadPre',
}

return M
