TelescopeMapArgs = TelescopeMapArgs or {}

local keymap = vim.keymap.set
local keymap_tele = function(mode, key, f, options, buffer)
    local desc = nil
    if type(options) == 'table' then
        desc = options.desc
        options.desc = nil
    end

    local rhs = function()
        local builtins = require('telescope.builtin')
        if builtins[f] then
            builtins[f](options or {})
        else
            require('plugins.telescope.handlers')[f](options or {})
        end
    end

    local map_options = {
        silent = true,
        buffer = buffer,
        desc = desc or string.format('Telescope %s', f),
    }

    vim.keymap.set(mode, key, rhs, map_options)
end

keymap_tele('n', '<F1>', 'help_tags', { desc = 'fzy help tags' })

keymap('n', '<c-p>', function()
    local builtins = require('telescope.builtin')
    if require('utils').is_git_dir() then
        builtins['git_files']({})
    else
        local find_command = nil

        if vim.fn.executable('bfs') == 1 then
            find_command = {
                '/usr/bin/bfs',
                '-type',
                'f',
                '-nocolor',
            }
        end

        builtins['find_files']({ find_command = find_command })
    end
end, { desc = 'fzy files' })
keymap_tele('n', '<leader>fp', 'find_files', { desc = 'fzy project files' })

keymap_tele('n', '<leader>;', 'buffers', { desc = 'fzy buffers' })
keymap_tele('n', '<leader>fM', 'man_pages', { desc = 'fzy manpages' })
keymap_tele('n', '<leader>fQ', 'loclist', { desc = 'fzy location list' })
keymap_tele('n', '<leader>fR', 'registers', { desc = 'fzy registers' })
keymap_tele('n', '<leader>fT', 'tags', { desc = 'fzy tags' })
keymap_tele(
    'n',
    '<leader>fb',
    'current_buffer_fuzzy_find',
    { desc = 'fzy current buffer' }
)
keymap_tele('n', '<leader>fc', 'commands', { desc = 'fzy commands' })
keymap_tele('n', '<leader>ff', 'grep_prompt', { desc = 'fzy find' })
keymap_tele('n', '<leader>fg', 'git_files', { desc = 'fzy git files' })
keymap_tele('n', '<leader>fh', 'oldfiles', { desc = 'fzy oldfiles' })
keymap_tele('n', '<leader>fk', 'keymaps', { desc = 'fzy keymaps' })
keymap_tele(
    'n',
    '<leader>fl',
    'live_grep',
    { debounce = 600, desc = 'fzy live grep' }
)
keymap_tele('n', '<leader>fm', 'marks', { desc = 'fzy marks' })
keymap_tele('n', '<leader>fo', 'vim_options', { desc = 'fzy vim options' })
keymap_tele('n', '<leader>fq', 'quickfix', { desc = 'fzy quickfix' })
keymap_tele(
    'n',
    '<leader>fs',
    'grep_string',
    { desc = 'fzy string under cursor' }
)
keymap_tele(
    'n',
    '<leader>ft',
    'current_buffer_tags',
    { desc = 'fzy tags (buffer)' }
)
keymap_tele(
    'n',
    '<leader>fx',
    'command_history',
    { desc = 'fzy command history' }
)
keymap_tele(
    'n',
    '<leader>fz',
    'spell_suggest',
    { desc = 'fzy spell suggestions under cursor' }
)

keymap_tele('n', '<leader>fw', 'grep_cword', { desc = 'fzy grep cword' })
keymap_tele('n', '<leader>fW', 'grep_cWORD', { desc = 'fzy grep cWORD' })
keymap_tele('n', '<leader>fr', 'grep_prompt', { desc = 'fzy grep prompt' })
keymap_tele(
    { 'n', 'v' },
    '<leader>fv',
    'grep_visual',
    { desc = 'fzy grep visual' }
)
keymap_tele('n', '<leader>f/', 'grep_last_search', {
    layout_strategy = 'vertical',
    desc = 'fzy grep last searchl',
})

keymap_tele('n', '<leader>fB', 'git_branches', { desc = 'fzy git branches' })
keymap_tele('n', '<leader>gC', 'git_commits', { desc = 'fzy git commits' })

-- keymap_tele('n', '<leader>lr', 'lsp_references', { desc = 'fzy lsp references' })
-- keymap_tele('n', '<leader>la', 'lsp_code_actions', { desc = 'fzy lsp code actions' })
-- keymap_tele('n', '<leader>lA', 'lsp_range_code_actions', { desc = 'fzy lsp range code action' })
-- keymap_tele('n', '<leader>ld', 'lsp_definitions', { desc = 'fzy lsp definitions' })
-- keymap_tele('n', '<leader>lm', 'lsp_implementations', { desc = 'fzy lsp implementations' })
-- keymap_tele('n', '<leader>lg', 'diagnostics', { buffer = true, desc = 'fzy lsp diagnostics (buffer)' })
-- keymap_tele('n', '<leader>lG', 'diagnostics', { desc = 'fzy lsp diagnostics' })
-- keymap_tele('n', '<leader>ls', 'lsp_document_symbols', { desc = 'fzy lsp document symbols' })
-- keymap_tele('n', '<leader>lS', 'lsp_workspace_symbols', { desc = 'fzy lsp workspace symbols' })

-- Telescope Meta
keymap_tele('n', '<leader>f?', 'builtin', { desc = 'fzy builtin' })

return keymap_tele
