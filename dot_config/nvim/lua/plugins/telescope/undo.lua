local keymap = vim.keymap.set

require('telescope').load_extension('undo')

keymap('n', '<leader>u', '<cmd>Telescope undo<cr>', { desc = 'show undo tree' })
