local treesitter_setup = function()

    -- Add tree-sitter-rpmspec development project
    if require('utils').is_dev('tree-sitter-rpmspec') then
        local rpmspec_path = '/home/asn/workspace/prj/oss/tree-sitter-rpmspec/asn-macro/'

        -- Add path for queries/hightlight.scm
        vim.opt.runtimepath:prepend(rpmspec_path)

        local augroup = vim.api.nvim_create_augroup('rpmspec', {})
        vim.api.nvim_create_autocmd('FileType', {
            group = augroup,
            pattern = { 'spec' },
            callback = function()
                vim.treesitter.language.register('rpmspec', 'spec')
                vim.bo.commentstring = "# %s"
                vim.bo.comments = "b:#"
            end,
        })
    end

    require('nvim-treesitter.configs').setup({
        ensure_installed = {
            'bash',
            'c',
            'cmake',
            'comment',
            'cpp',
            'css',
            'diff',
            'dockerfile',
            'doxygen',
            'git_config',
            'git_rebase',
            'gitcommit',
            'gitignore',
            'go',
            'gomod',
            'gosum',
            'html',
            'ini',
            'jq',
            'javascript',
            'json',
            'json5',
            'lua',
            'make',
            'markdown',
            'markdown_inline',
            'meson',
            'norg',
            'objdump',
            'pem',
            'perl',
            'php',
            'python',
            'qmljs',
            'query',
            'regex',
            'ruby',
            'rust',
            'rst',
            'scss',
            'ssh_config',
            'strace',
            'toml',
            'typescript',
            'vim',
            'vimdoc',
            'yaml',
        },
        highlight = {
            enable = true, -- false will disable the whole extension
            additional_vim_regex_highlighting = false,
        },
        incremental_selection = {
            enable = true,
        },
        indent = {
            -- This will set 'indentexpr' and overwrite smartindent/cindent
            enable = true,
        },
        textobjects = {
            enable = true,
            select = {
                enable = true,
                keymaps = {
                    ['ac'] = '@comment.outer',
                    ['ic'] = '@comment.outer',
                    ['ao'] = '@class.outer',
                    ['io'] = '@class.inner',
                    ['af'] = '@function.outer',
                    ['if'] = '@function.inner',
                },
            },
            move = {
                enable = true,
                set_jumps = true,
                goto_next_start = {
                    [']C'] = { query = '@class.outer', desc = 'next class' },
                    [']f'] = {
                        query = '@function.outer',
                        desc = 'next function',
                    },
                    [']p'] = {
                        query = '@parameter.outer',
                        desc = 'next parameter',
                    },
                },
                goto_previous_start = {
                    [']C'] = { query = '@class.outer', desc = 'prev class' },
                    [']f'] = {
                        query = '@function.outer',
                        desc = 'prev function',
                    },
                    [']p'] = {
                        query = '@parameter.outer',
                        desc = 'prev parameter',
                    },
                },
            },
        },
        endwise = {
            enable = true,
        },
    })
end

local M = {
    {
        'nvim-treesitter/nvim-treesitter',
        cond = require('utils').have_compiler,
        dependencies = {
            'nvim-treesitter/nvim-treesitter-context',
            'nvim-treesitter/nvim-treesitter-textobjects',
            'RRethy/nvim-treesitter-endwise',
        },
        event = 'BufNewFile',
        build = function()
            if require('utils').have_compiler() then
                vim.cmd('TSUpdate')
            end
        end,
        config = treesitter_setup,
    },
}

return M
