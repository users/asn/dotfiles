local M = {
    'beauwilliams/focus.nvim',
    lazy = false,
    dev = require('utils').is_dev('focus.nvim'),
}

function M.config()
    local keymap = vim.keymap.set

    -- Let focus handle them
    vim.o.number = false
    vim.o.relativenumber = false

    require('focus').setup({
        commands = false,
        ui = {
            hybridnumber = true,
            cursorline = true,
            signcolumn = true,
        },
    })

    -- Split windows nicely
    keymap('n', '<c-l>', function()
        require('focus').split_nicely('')
    end, { silent = true, desc = 'split nicely' })

    -- Disable focus
    local ignore_filetypes = { 'neo-tree' }
    local ignore_buftypes = { 'nofile', 'prompt', 'popup' }

    local augroup =
        vim.api.nvim_create_augroup('FocusDisable', { clear = true })

    vim.api.nvim_create_autocmd('WinEnter', {
        group = augroup,
        callback = function(_)
            if vim.tbl_contains(ignore_buftypes, vim.bo.buftype) then
                vim.w.focus_disable = true
            end
        end,
        desc = 'Disable focus autoresize for BufType',
    })

    vim.api.nvim_create_autocmd('FileType', {
        group = augroup,
        callback = function(_)
            if vim.tbl_contains(ignore_filetypes, vim.bo.filetype) then
                vim.w.focus_disable = true
            end
        end,
        desc = 'Disable focus autoresize for FileType',
    })
end

return M
