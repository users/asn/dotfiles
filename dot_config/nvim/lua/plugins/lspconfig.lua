local M = {
    'neovim/nvim-lspconfig',
    event = 'BufReadPre',
    dependencies = {
        'hrsh7th/cmp-nvim-lsp',
    },
}

function M.config()
    require('lspsaga')
    require('lsp')
end

return M
