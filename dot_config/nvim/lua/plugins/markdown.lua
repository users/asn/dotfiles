local M = {
    'MeanderingProgrammer/markdown.nvim',
    name = 'makrdown-render',
    dependencies = { 'nvim-treesitter/nvim-treesitter' },
    event = { 'FileType' },
}

function M.config()
    local keymap = vim.keymap.set

    require('render-markdown').setup({
        highlights = {
            heading = {
                -- Background of heading line
                backgrounds = { '@diff.plus', '@diff.delta', '@diff.minus' },
                -- Foreground of heading character only
                foregrounds = {
                    '@markup.heading.1',
                    '@markup.heading.2',
                    '@markup.heading.3',
                    '@markup.heading.4',
                    '@markup.heading.5',
                    '@markup.heading.6',
                },
            },
        },
    })

    keymap('n', '<leader>M', function()
        require('render-markdown').toggle()
    end, { desc = 'render markdown' })
end

return M
