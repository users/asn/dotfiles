local M = {
    'folke/trouble.nvim',
    dependencies = {
        'nvim-tree/nvim-web-devicons',
    },
    event = 'LspAttach',
    version = '*', -- use stable branch
}

function M.config()
    local keymap = vim.keymap.set

    require('trouble').setup()

    keymap(
        'n',
        '<leader>xx',
        '<cmd>Trouble diagnostics toggle filter.buf=0<cr>',
        { silent = true, desc = 'diagnostics for current buffer' }
    )
    keymap(
        'n',
        '<leader>xp',
        '<cmd>Trouble diagnostics toggle<cr>',
        { silent = true, desc = 'diagnostics for complete project' }
    )
    -- keymap(
    --     'n',
    --     '<leader>xw',
    --     '<cmd>TroubleToggle workspace_diagnostics<cr>',
    --     { silent = true, desc = 'diagnostics for workspace' }
    -- )
    -- keymap(
    --     'n',
    --     '<leader>xl',
    --     '<cmd>TroubleToggle loclist<cr>',
    --     { silent = true, desc = 'diagnostics for location list' }
    -- )
    -- keymap(
    --     'n',
    --     '<leader>xq',
    --     '<cmd>TroubleToggle quickfix<cr>',
    --     { silent = true, desc = 'diagnostics for quickfix list' }
    -- )
    keymap(
        'n',
        'gR',
        '<cmd>Trouble lsp toggle focus=false win.position=right<cr>',
        { silent = true, desc = 'show lsp references' }
    )
end

return M
