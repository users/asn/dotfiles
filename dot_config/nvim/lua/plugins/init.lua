return {
    {
        'folke/lazy.nvim',
        version = '*', -- use stable branch
    },
    {
        'folke/tokyonight.nvim',
        dev = require('utils').is_dev('tokyonight.nvim'),
    },
    {
        'cryptomilk/nightcity.nvim',
        lazy = false,
        priority = 1000,
        config = function()
            require('color.nightcity')
        end,
        dev = require('utils').is_dev('nightcity.nvim'),
    },
    {
        "vhyrro/luarocks.nvim",
        priority = 1000,
        config = true,
    },
    {
        'nvim-lua/plenary.nvim',
    },
    {
        'nvim-tree/nvim-web-devicons',
        config = function()
            require('nvim-web-devicons').setup()
        end,
    },
    {
        'antoinemadec/FixCursorHold.nvim',
        config = function()
            vim.g.cursorhold_updatetime = 100
        end
    },
}
