local M = {
    'echasnovski/mini.nvim',
    -- VeryLazy hides splash screen
    event = { 'BufReadPost', 'InsertEnter' },
    -- dev = require('utils').is_dev('mini.nvim'),
}

function M.config()
    if vim.fn.has('nvim-0.9') == 1 then
        require('plugins.mini.animate')
    end
    require('plugins.mini.indent')
    require('plugins.mini.misc')
    require('plugins.mini.move')
    require('plugins.mini.statusline')
    require('plugins.mini.surround')

    require('mini.jump2d').setup()
    require('mini.splitjoin').setup()
end

return M
