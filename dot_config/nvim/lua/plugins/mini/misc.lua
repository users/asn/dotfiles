require('mini.misc').setup()

require('mini.misc').setup_restore_cursor({
    ignore_buftype = { 'quickfix', 'nofile', 'help' },
    ignore_filetype = { 'gitcommit', 'gitrebase', 'svn', 'hgcommit' },
    center = true,
})
