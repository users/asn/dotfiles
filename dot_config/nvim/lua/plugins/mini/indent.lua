require('mini.indentscope').setup({
    draw = {
        -- Delay (in ms) between event and start of drawing scope indicator
        delay = 80,
    },
    symbol = '┆',
})

vim.api.nvim_set_hl(0, 'MiniIndentscopeSymbol', { fg = '#de5285' })

-- Disable mini.indentscope for certain filetypes
local ignore_filetype = { 'sagacodeaction' }
local augroup =
    vim.api.nvim_create_augroup('mini_indentscope_ft', { clear = true })

vim.api.nvim_create_autocmd('FileType', {
    group = augroup,
    callback = function(_)
        if vim.tbl_contains(ignore_filetype, vim.bo.filetype) then
            vim.b.miniindentscope_disable = true
        end
    end,
    desc = 'Disable mini.indentscope for certain filetypes',
})

-- Create keymap for toggling indentscope
local toggle = function(bufnr)
    local indent = require('mini.indentscope')
    if bufnr then
        vim.b.miniindentscope_disable = not vim.b.miniindentscope_disable
    else
        vim.g.miniindentscope_disable = not vim.g.miniindentscope_disable
    end
    indent.auto_draw({ lazy = true })
end

local btoggle = function()
    toggle(vim.api.nvim_get_current_buf())
end

vim.keymap.set(
    '',
    '<leader>"',
    "<cmd>lua require('plugins.mini.indent').btoggle()<CR>",
    { silent = true, desc = 'Toggle indent line' }
)

return {
    btoggle = btoggle,
}
