local M = {
    'nvim-telescope/telescope-fzy-native.nvim',
    lazy = true,
}

M.build = function(plugin)
    if require('utils').have_compiler() then
        local process = require('lazy.manage.process')
        local opts = {
            env = { 'CFLAGS="-Ofast -march=native -flto=auto"' },
            cwd = plugin.dir .. '/deps/fzy-lua-native/',
            on_line = function(line)
                print(line)
            end,
        }

        process.spawn('make', opts)
    end
end

return M
