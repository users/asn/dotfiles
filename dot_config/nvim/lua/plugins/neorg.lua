M = {
    'nvim-neorg/neorg',
    dependencies = { "luarocks.nvim" },
    version = '*',
    event = 'UiEnter',
}

M.config = function()
    vim.o.conceallevel = 2 -- Concealed text will be completely hidden
    vim.opt.concealcursor = 'nc' -- Modes text can be concealed under cursor

    require('neorg').setup({
        load = {
            ['core.defaults'] = {}, -- Loads default behaviour
            ['core.concealer'] = { -- Adds pretty icons to your documents
                config = {
                    icons = {
                        todo = {
                            uncertain = {
                                icon = '󱔢',
                            },
                        },
                    },
                },
            },
            ['core.dirman'] = { -- Manages Neorg workspaces
                config = {
                    workspaces = {
                        notes = '~/Notes/personal',
                        oss = '~/Notes/oss',
                        work = '~/Notes/work',
                        ig = '~/Notes/igklettern',
                    },
                    default_workspace = 'notes',
                },
            },
            ['core.export'] = {}, -- Exports to other formats
        },
    })
end

return M
