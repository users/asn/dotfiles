local keymap = vim.keymap.set

if vim.fn.has('nvim-0.10') == 0 then
    vim.o.pastetoggle = '<F2>' -- Toggle insert mode
end

-- Keep matches center screen when cycling with n|N
keymap('n', 'n', 'nzzzv', { desc = "fwd  search '/' or '?'" })
keymap('n', 'N', 'Nzzzv', { desc = "back search '/' or '?'" })

-- Half page up/down with centering
keymap('n', '<c-u>', '<c-u>zz')
keymap('n', '<c-d>', '<c-d>zz')

-- Let the cursor stay where it is for joins
keymap('n', 'J', 'mzJ`v', { desc = 'join lines' })

-- Any jump over 5 modifies the jumplist
-- so we can use <C-o> <C-i> to jump back and forth
for _, c in ipairs({ 'j', 'k' }) do
    keymap(
        'n',
        c,
        ([[(v:count > 0 ? "m'" . v:count : "") . '%s']]):format(c),
        { expr = true, silent = true }
    )
end

-- Write file if anything changed
keymap(
    { 'n', 'v', 'i' },
    '<c-s>',
    '<esc>:update<cr>',
    { silent = true, desc = 'save buffer' }
)

-- Change case
keymap(
    'n',
    '<F2>',
    ':setlocal ignorecase! ignorecase?<CR>',
    { desc = 'toogle ignorecase' }
)

-- Manpage
keymap(
    'n',
    '<leader>P',
    function()
        vim.cmd.Man(vim.fn.expand('<cword>'))
    end,
    { desc = 'Open manpage under cursor' })

-- Tab navigation
keymap('n', '<Leader>tp', ':tabprevious<CR>', { desc = 'previous tab' })
keymap('n', '<Leader>tn', ':tabnext<CR>', { desc = 'next tab' })
keymap('n', '<Leader>tf', ':tabfirst<CR>', { desc = 'first tab' })
keymap('n', '<Leader>tl', ':tablast<CR>', { desc = 'last tab' })
keymap('n', '<Leader>tN', ':tabnew<CR>', { desc = 'new tab' })
keymap('n', '<Leader>tc', ':tabclose<CR>', { desc = 'close tab' })

-- Navigate buffers
keymap('n', '[b', ':bprevious<CR>', { desc = 'previous buffer' })
keymap('n', ']b', ':bnext<CR>', { desc = 'next buffer' })
keymap('n', '[B', ':bfirst<CR>', { desc = 'first buffer' })
keymap('n', ']B', ':blast<CR>', { desc = 'last buffer' })

-- Quickfix list mappings
keymap(
    'n',
    '<leader>q',
    "<cmd>lua require'utils'.toggle_qf('q')<CR>",
    { desc = 'toogle quickfix list' }
)
keymap('n', '[q', ':cprevious<CR>', { desc = 'previous qf' })
keymap('n', ']q', ':cnext<CR>', { desc = 'next qf' })
keymap('n', '[Q', ':cfirst<CR>', { desc = 'first qf' })
keymap('n', ']Q', ':clast<CR>', { desc = 'last qf' })

-- Location list mappings
keymap(
    'n',
    '<leader>Q',
    "<cmd>lua require'utils'.toggle_qf('l')<CR>",
    { desc = 'toogle locationlist' }
)
keymap('n', '[l', ':lprevious<CR>', { desc = 'previous loc' })
keymap('n', ']l', ':lnext<CR>', { desc = 'next loc' })
keymap('n', '[L', ':lfirst<CR>', { desc = 'first loc' })
keymap('n', ']L', ':llast<CR>', { desc = 'last loc' })

-- Tags / Preview tags
keymap('n', '[t', ':tprevious<CR>', { desc = 'previous tag' })
keymap('n', ']t', ':tNext<CR>', { desc = 'next tag' })
keymap('n', '[T', ':tfirst<CR>', { desc = 'first tag' })
keymap('n', ']T', ':tlast<CR>', { desc = 'last tag' })
keymap('n', '[p', ':ptprevious<CR>', { desc = 'previous tag (preview)' })
keymap('n', ']p', ':ptnext<CR>', { desc = 'next tag (preview)' })

-- Copy/paste with system clipboard
keymap({ 'n', 'x' }, 'gy', '"+y', { desc = 'copy to system clipboard' })
keymap({ 'n', 'x' }, 'gY', '"+y$', { desc = 'Copy to system clipboard (EOL)' })
keymap('n', 'gp', '"+p', { desc = 'paste after from system clipboard)' })
keymap('n', 'gP', '"+P', { desc = 'paste before from system clipboard)' })
-- - Paste in Visual with `P` to not copy selected text (`:h v_P`)
keymap('x', 'gp', '"+_dP', { desc = 'replace from system clipboard' })

-- Paste from secondary clipboard
-- gS is mapped from mini.splitjoin
--
-- keymap(
--     { 'n', 'v' },
--     'gs',
--     '"*p',
--     { desc = 'paste after (clipboard secondary)' }
-- )
-- keymap(
--     { 'n', 'v' },
--     'gS',
--     '"*P',
--     { desc = 'paste before (clipboard secondary)' }
-- )

-- Replace selection
keymap(
    { 'x' },
    '<leader>p',
    '"_dp',
    { desc = 'replace selection (without yanking)' }
)

-- paste from 0 register
keymap({ 'n', 'v' }, '<leader>0p', '"0p', { desc = 'paste (register 0)' })
keymap(
    { 'n', 'v' },
    '<leader>0P',
    '"0P',
    { desc = 'paste before cursor (register 0)' }
)

-- keep visual selection when (de)indenting
keymap('v', '<', '<gv')
keymap('v', '>', '>gv')

-- Search and Replace
-- 'c.' for word, '<leader>c.' for WORD
-- 'c.' in visual mode for selection
keymap(
    'n',
    'c.',
    [[:%s/\<<C-r><C-w>\>//g<Left><Left>]],
    { desc = 'search and replace word under cursor' }
)
keymap(
    'n',
    '<leader>c.',
    [[:%s/\V<C-r><C-a>//g<Left><Left>]],
    { desc = 'search and replace WORD under cursor' }
)
keymap(
    'x',
    '<leader>c.',
    [[:<C-u>%s/<C-r>=luaeval("require'utils'.get_visual_selection(true)")<CR>//g<Left><Left>]],
    { desc = 'seach and replace selection' }
)

-- Overloads for 'd|c' that don't pollute the unnamed registers
keymap('n', '<leader>D', '"_D', { desc = "blackhole 'D'" })
keymap('n', '<leader>C', '"_C', { desc = "blackhole 'C'" })

-- Toggle colored column at 81
keymap(
    'n',
    '<leader>|',
    ':execute "set colorcolumn=" . (&colorcolumn == "" ? "81" : "")<CR>',
    { silent = true, desc = 'toggle color column on/off' }
)

-- Map <leader>o & <leader>O to newline without insert mode
keymap(
    'n',
    '<leader>o',
    ':<C-u>call append(line("."), repeat([""], v:count1))<CR>',
    { silent = true, desc = 'newline below (no insert-mode)' }
)
keymap(
    'n',
    '<leader>O',
    ':<C-u>call append(line(".")-1, repeat([""], v:count1))<CR>',
    { silent = true, desc = 'newline above (no insert-mode)' }
)

-- w!! to save with sudo
-- This creates more issues thant it solves, it triggers also with / (search)
--[[
keymap(
    'c',
    'w!!',
    '<esc>:lua require("utils").sudo_write()<CR>',
    { silent = true, desc = 'Save file with sudo' }
)
]]
