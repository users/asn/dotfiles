local o = vim.o -- Get or set options. Like :set.
local opt = vim.opt -- Get or set list and mapstyle options

-----------------------------------------------------------
-- MAPPINGS
-----------------------------------------------------------

-- Set the mapleader as early as possible
-- https://coderwall.com/p/gdowew/vim-tip-map-leader-to-space
local keymap = vim.keymap.set
keymap('n', '<space>', '<nop>')
keymap('x', '<space>', '<nop>')
vim.g.mapleader = ' ' -- set map leader to space
-- With mini.jump we don't need ',' as f can be used ...
vim.g.maplocalleader = ',' -- set localleader to ','

-----------------------------------------------------------
-- PROVIDERS
-----------------------------------------------------------

--
-- Disable providers we do not care a about
--
vim.g.loaded_python_provider = 0 -- disable python2 support
vim.g.loaded_python3_provider = 0 -- disable python3 support
vim.g.loaded_ruby_provider = 0 -- disable ruby support
vim.g.loaded_perl_provider = 0 -- disable perl support
vim.g.loaded_node_provider = 0 -- disable nodejs support

-----------------------------------------------------------
-- FILES AND BACKUP
-----------------------------------------------------------

o.undofile = true -- Enable persistent undo (see also `:h undodir`)
o.backup = false -- no backup file
o.writebackup = false -- do not backup file before write
o.encoding = 'utf-8'
o.fileencoding = 'utf-8'
o.fileformat = 'unix' -- <nl> for EOL

-----------------------------------------------------------
-- TEXT FORMATTING
-----------------------------------------------------------

o.joinspaces = true -- insert spaces after '.?!' when joining lines
o.breakindent = true -- preserve horizontal blocks of text
-- The indent options are normally overwritten by tree-sitter setting
-- 'identexpr'!
-- o.autoindent = true -- copy indent from current line on newline
o.smartindent = true -- like 'autoindent' but also recognizes C syntax
-- opt.cindent = true -- Works more cleverly than the other two and is confgiureable

o.tabstop = 4 -- Tab indentation levels every two columns
o.softtabstop = 4 -- Tab indentation when mixing tabs & spaces
o.shiftwidth = 4 -- Indent/outdent by two columns
o.shiftround = true -- Always indent/outdent to nearest tabstop
o.expandtab = true -- Convert all tabs that are typed into spaces
-- See 'listchars' to make Tabs visible!
o.smarttab = true -- Use tabs at the start of a line, spaces elsewhere

-- Make special characters visible to use on ':set list'
o.list = true
opt.listchars = {
    tab = '»·',
    trail = '•',
    extends = '…',
    nbsp = '␣',
}

opt.fillchars = {
    fold = '-',
}

o.showbreak = '↻ ' -- String to put at the start of lines that have been wrapped.

-- Options for the "text format" command ("gq")
--
-- t: format code
-- c: auto-wrap comments using textwidth
-- r: auto-insert the current comment leader after hitting <Enter>
-- o: auto-insert the current comment leader after hitting 'o' or 'O'
-- q: allow formatting comments with 'gq'
-- n: recognize numbered lists
-- j: remove comment leader when it makes sense
opt.formatoptions = opt.formatoptions + 't' + 'c' + 'r' + 'q' + 'n' + 'j'

-- Allow jump commands for left/right motion to wrap to previous/next line when
-- cursor is on first/last character in the line:
opt.whichwrap:append('<,>,[,]')

o.linebreak = true -- do not break words on line wrap
o.wrap = false -- do not wrap long lines

vim.g.colorcolumn = '+1' -- set colorcolumn to textwidth + 1
o.colorcolumn = vim.g.colorcolumn

-----------------------------------------------------------
-- COLORS
-----------------------------------------------------------

o.background = 'dark'
o.termguicolors = true

-----------------------------------------------------------
-- SESSION DATA HISTORY
-----------------------------------------------------------

--  ! - Save and restore global variables (their names should be without lowercase letter).
--  ' - Specify the maximum number of marked files remembered. It also saves the jump list and the change list.
--  < - Maximum of lines saved for each register. All the lines are saved if this is not included, <0 to disable pessistent registers.
--  % - Save and restore the buffer list. You can specify the maximum number of buffer stored with a number.
--  / or : - Number of search patterns and entries from the command-line history saved. opt.history is used if it’s not specified.
--  f - Store file (uppercase) marks, use 'f0' to disable.
--  s - Specify the maximum size of an item’s content in KiB (kilobyte).
--      For the viminfo file, it only applies to register.
--      For the shada file, it applies to all items except for the buffer list and header.
--  h - Disable the effect of 'hlsearch' when loading the shada file.
o.shada = [[!,'1000,<10000,s500,:100,/100,h]]

-- See autocmds how to reset the cursor postion when reopening a file

-----------------------------------------------------------
-- NEOVIM UI
-----------------------------------------------------------

o.backspace = 'indent,eol,start'
o.number = true -- show line numbers
o.relativenumber = true -- show relative numbers from cursor postion
o.ruler = true -- show line,col at the cursor pos
o.showcmd = true -- show current command under the cmd line
o.showfulltag = true -- auto completion (great for programming)
o.showmode = true -- show current mode (insert, etc) under the cmdline
o.splitbelow = true -- ':new' ':split' below current
o.splitright = true -- ':vnew' ':vsplit' right of current
o.splitkeep = 'cursor' -- Reduce scroll during window split
o.scrolloff = 3 -- min number of lines to keep between cursor and screen edge
o.sidescrolloff = 2 -- min number of cols to keep between cursor and screen edge
o.mouse = '' -- disable the mouse
o.guicursor = ''
o.foldenable = false -- open all folds
o.winminheight = 1 -- Minimum hiehgt for a window
o.updatetime = 3000 -- Set a shorter update time for gitsigns
o.laststatus = 2 -- Always show statusline

-----------------------------------------------------------
-- VISUAL CLUES
-----------------------------------------------------------

o.errorbells = false -- disable error bells (no beep/flash)
o.hlsearch = true -- highlight all text matching current search pattern
o.incsearch = true -- show search matches as you type
o.inccommand = 'nosplit' -- show search and replace in real time
o.smartcase = true -- case sensitive when search includes uppercase
o.ignorecase = true -- Ignore case when searching (use `\C` to force not doing that)
o.visualbell = true
o.wildmenu = true
o.wildmode = 'longest:full,full' -- Use the cool tab complete menu
--opt.wildmode          = 'list:longest,full' -- Use the cool tab complete menu
o.wildoptions = 'pum' -- Show completion items using the pop-up-menu (pum)

-- Set completeopt to have a better completion experience
o.completeopt = 'noinsert,menuone,noselect'
-- Avoid showing message extra message when using completion
opt.shortmess:append('c')
opt.shortmess:append('C')
o.virtualedit = 'block' -- Allow going past the end of line in visual block mode

-- DON'T USE CLIPBOARD, USE SMARTYANK!
-- unnamed     = use the " register (cmd-s paste in our term)
-- unnamedplus = use the + register (cmd-v paste in our term)
-- o.clipboard = 'unamedplus'

-----------------------------------------------------------
-- SPELLING
-----------------------------------------------------------

o.dictionary = '/usr/share/dict/words'

-----------------------------------------------------------
-- SPELLING
-----------------------------------------------------------

-- Use ripgrep if available
if require('utils').shell_command('rg') then
    opt.grepprg = '/usr/bin/rg --vimgrep --no-heading --smart-case --hidden'
    opt.grepformat = '%f:%l:%c:%m'
end

-----------------------------------------------------------
--  PLUGINS
-----------------------------------------------------------
local disabled_built_ins = {
    '2html_plugin',
    'fzf',
    'getscript',
    'getscriptPlugin',
    'gitcommit',
    'logipat',
    -- 'netrw',
    -- 'netrwFileHandlers',
    -- 'netrwPlugin',
    -- 'netrwSettings',
    'rrhelper',
    'spellfile_plugin',
    'tar',
    'tarPlugin',
    'vimball',
    'vimballPlugin',
    'zip',
    'zipPlugin',
}
for _, plugin in pairs(disabled_built_ins) do
    vim.g['loaded_' .. plugin] = 1
end
