if io.open('cscope.out', 'r') ~= nil then
    -- define key table for input strings
    local sym_map = {
        ['s'] = 'Find this symbol (cscope)',
        ['g'] = 'Find this global definition (cscope)',
        ['c'] = 'Find functions calling this function (cscope)',
        ['t'] = 'Find this text string (cscope)',
        ['e'] = 'Find this egrep pattern (cscope)',
        ['f'] = 'Find this file (cscope)',
        ['i'] = 'Find files #including this file (cscope)',
        ['d'] = 'Find functions called by this function (cscope)',
        ['v'] = 'Find places where this symbol is assigned a value (cscope)',
    }

    -- use both cscope and ctag for 'ctrl-]', ':ta', and 'vim -t'
    vim.opt.cscopetag = true
    -- check cscope for definition of a symbol before checking ctags: set to 1
    -- if you want the reverse search order.
    vim.opt.csto = 0
    -- show msg when cscope db added
    vim.opt.cscopeverbose = true
    -- results in quickfix window
    vim.opt.cscopequickfix = 's-,g-,c-,t-,e-,f-,i-,d-,a-'
    -- add cscope database in current directory
    vim.cmd('cscope add cscope.out')

    -- function to print xcscpoe.el like prompts
    CScope_Search = function(operation, default_symbol)
        local new_symbol = vim.fn.input(
            sym_map[operation] .. " (default: '" .. default_symbol .. "'): "
        )
        if '' ~= new_symbol then
            vim.cmd(':cscope find ' .. operation .. ' ' .. new_symbol)
        else
            vim.cmd(':cscope find ' .. operation .. ' ' .. default_symbol)
        end
        vim.cmd('copen')
    end

    -- Mappings
    local keymap = vim.keymap.set
    -- Add leader shortcuts
    keymap(
        'n',
        '<leader>cs',
        [[<cmd>lua CScope_Search('s', vim.fn.expand("<cword>"))<cr>]],
        { silent = true, desc = sym_map['s'] }
    )
    keymap(
        'n',
        '<leader>cg',
        [[<cmd>lua CScope_Search('g', vim.fn.expand("<cword>"))<cr>]],
        { silent = true, desc = sym_map['g'] }
    )
    keymap(
        'n',
        '<leader>cc',
        [[<cmd>lua CScope_Search('c', vim.fn.expand("<cword>"))<cr>]],
        { silent = true, desc = sym_map['c'] }
    )
    keymap(
        'n',
        '<leader>ct',
        [[<cmd>lua CScope_Search('t', vim.fn.expand("<cword>"))<cr>]],
        { silent = true, desc = sym_map['t'] }
    )
    keymap(
        'n',
        '<leader>ce',
        [[<cmd>lua CScope_Search('e', vim.fn.expand("<cword>"))<cr>]],
        { silent = true, desc = sym_map['e'] }
    )
    keymap(
        'n',
        '<leader>cf',
        [[<cmd>lua CScope_Search('f', vim.fn.expand("<cfile>"))<cr>]],
        { silent = true, desc = sym_map['f'] }
    )
    keymap(
        'n',
        '<leader>ci',
        [[<cmd>lua CScope_Search('i', vim.fn.expand("<cfile>"))<cr>]],
        { silent = true, desc = sym_map['i'] }
    )
    keymap(
        'n',
        '<leader>cd',
        [[<cmd>lua CScope_Search('d', vim.fn.expand("<cword>"))<cr>]],
        { silent = true, desc = sym_map['d'] }
    )
    keymap(
        'n',
        '<leader>cv',
        [[<cmd>lua CScope_Search('a', vim.fn.expand("<cword>"))<cr>]],
        { silent = true, desc = sym_map['v'] }
    )
end
