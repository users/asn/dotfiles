local Lazy = {}
Lazy.__index = Lazy

local install_path = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'

function Lazy:is_installed()
    local uv = vim.loop or vim.uv
    local state = uv.fs_stat(install_path)
    if not state then
        return false
    end

    return true
end

function Lazy:init()
    if self:is_installed() then
        vim.opt.rtp:prepend(install_path)
        return
    end

    if vim.fn.input('Install folke/lazy.nvim? (y/n) ') == 'y' then
        vim.fn.system({
            'git',
            'clone',
            '--filter=blob:none',
            'https://github.com/folke/lazy.nvim.git',
            '--branch=stable', -- latest stable release
            install_path,
        })
        print(' => Successfully installed lazy.nvim.')
    end

    vim.opt.rtp:prepend(install_path)
end

function Lazy:load()
    local ok, lazy = pcall(require, 'lazy')
    if not ok then
        require('utils').error('Failed to load folke/lazy.nvim')
        return
    end

    lazy.setup('plugins', {
        defaults = {
            lazy = true,
        },
        install = {
            colorscheme = { 'gruvbox' },
        },
        checker = {
            enabled = false,
        },
        ui = {
            border = 'rounded',
            custom_keys = {
                ['<localleader>l'] = false,
                ['<localleader>t'] = false,
            },
        },
        dev = {
            path = '~/workspace/prj/oss',
        },
        debug = false,
    })

    local keymap = vim.keymap.set

    keymap('n', '<leader>L', '<cmd>Lazy<cr>', { desc = 'lazy plugin manager' })
end

local M = {}

function M.init()
    Lazy:init()
    Lazy:load()
end

return M
