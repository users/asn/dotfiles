local augroup = vim.api.nvim_create_augroup('ftdetect', {})

local detect_file_type = function(pattern, filetype)
    vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile', 'StdinReadPost' }, {
        group = augroup,
        pattern = pattern,
        callback = function(ev)
            vim.api.nvim_buf_call(ev.buf, function()
                vim.api.nvim_cmd({ cmd = 'setfiletype', args = { filetype } }, {})
            end)
        end,
    })
end

detect_file_type('*.qml', 'qml')
detect_file_type('wscript*', 'python')
detect_file_type({ '*.fmf', '*.sls' }, 'yaml')
detect_file_type({ '/tmp/vc.*', '*.changes' }, 'changes')
