require('nightcity').setup({
    on_highlights = function(groups, _)
        groups['@lsp.typemod.parameter.readonly'] = { italic = true }
        groups['@lsp.typemod.variable.readonly'] = { italic = true }
    end,
})

vim.cmd.colorscheme('nightcity')
