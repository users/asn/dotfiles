local c = {
    magenta = '#de5285',
    str_fg = '#f9efc5',
    str_bg = '#4a4542',
}

require('gruvbox').setup({
    contrast = '', -- can be "hard" or "soft"
    bold = true,
    italic = {
        strings = true,
        comments = true,
    },
    inverse = true, -- invert background for search, diffs, statuslines and errors
    invert_selection = false,
    invert_signs = false,
    invert_tabline = false,
    invert_intend_guides = false,
    undercurl = true,
    underline = true,

    overrides = {
        String = { fg = c.str_fg, bg = c.str_bg },
        Number = { fg = c.magenta },
        Float = { fg = c.magenta },
        Boolean = { fg = c.magenta },
        ['@lsp.typemod.variable.readonly'] = { italic = true },
    },
})

vim.cmd.colorscheme('gruvbox')
