# cryptomilk neovim lua setup

## Required packages

* [neovim](https://neovim.io) >= 0.5.0
* [Nerdfont](https://www.nerdfonts.com/)

## Optional packages

* [hunspell](http://hunspell.github.io/)
* [editorconfig](https://editorconfig.org)
* [clangd](https://clangd.llvm.org/)
* [rust language server (rls)](https://github.com/rust-lang/rls)
* [python-language-server](https://github.com/palantir/python-language-server)
* [bash-language-server](https://github.com/bash-lsp/bash-language-server)
* [diagnostic-language-server](https://github.com/iamcco/diagnostic-languageserver)
* [shellcheck](https://www.shellcheck.net/)
* [glow](https://github.com/charmbracelet/glow)

openSUSE:

    zypper install editorconfig clang glow rls hunspell hunspell-en-US

Fedora:

    dnf install editorconfig clang-tools-extra rls hunspell hunspell-en-US
