set horizontal-scroll = 33%
set line-graphics = yes

### Options
# Use Git's default (reverse chronological) order, never automatically
# use topo-order for the commit graph
set commit-order = default

# Limit number of commits loaded by default to 10000
set main-options = --max-count=30000

### Bindings
bind generic X !git cherry-pick -x %(commit)
bind generic S !git show %(commit)
bind generic R !git revert %(commit)

bind generic V edit

# Vim Style
bind diff <Up> move-up
bind diff <Down> move-down

bind status <Up> move-up
bind status <Down> move-down

bind stage <Up> move-up
bind stage <Down> move-down

bind generic h scroll-left
bind generic j move-down
bind generic k move-up
bind generic l scroll-right

bind generic <Ctrl-f> scroll-page-down
bind generic <Ctrl-b> scroll-page-down

bind generic g  none
bind generic gg move-first-line
bind generic gj next
bind generic gk previous
bind generic gp parent
bind generic gP back
bind generic gn view-next

bind main    G move-last-line
bind generic G move-last-line

bind generic <ShiftLeft> :toggle split-view-width -5%
bind generic <ShiftRight> :toggle split-view-width +5%

# Views
set blame-view = date:default author:full file-name:auto id:yes,color line-number:no,interval=5 text
set grep-view = file-name:no line-number:yes,interval=1 text
set main-view = line-number:no,interval=5 id:yes date:default author:full,width=18 commit-title:yes,graph,refs,overflow=no
set refs-view = date:default author:full ref commit-title
set stash-view = line-number:no,interval=5 id:no date:default author:full commit-title
set status-view = line-number:no,interval=5 status:short file-name
set tree-view = line-number:no,interval=5 mode author:full file-size date:default id:no file-name

set split-view-width = 65%

### Colors

# general
color   default                 15      235
color   cursor                  15      241
color   title-focus             242     221
color   title-blur              242     221
color   delimiter               213     default
color   author                  156     default
color   date                    81      default
color   line-number             221     default
color   mode                    255     default

# main
color   main-tag                213     default     bold
color   main-local-tag          213     default
color   main-remote             221     default
color   main-replace            81      default
color   main-tracked            221     default     bold
color   main-ref                81      default
color   main-head               213     default     bold
color   graph-commit            226     default
# color   main_revgraph           81      default

# status
#color  stat-head       81      default

# Diff   colors
color   diff_add                10      default
color   diff_add2               10      default
color   diff_del                196     default
color   diff_del2               196     default
color   diff-header             221     default
color   diff-index              81      default
color   diff-chunk              213     default
color   diff_oldmode            221     default
color   diff_newmode            221     default
color   'deleted file mode'     221     default
color   'copy from'             223     default
color   'copy to'               221     default
color   'rename from'           221     default
color   'rename to'             221     default
color   diff_similarity         221     default
color   'dissimilarity '        221     default
color   'diff-tree '            81      default
color   diff-stat               81      default
color   'Reported-by:'          156     default

color   'Author:'               156     default
color   'Commit:'               213     default
color   'AuthorDate:'           221     default
color   'CommitDate:'           221     default
color   'Date:'                 81      default
color   pp_refs                 213     default
color   palette-0               226     default
color   palette-1               213     default
color   palette-2               118     default
color   palette-3               51      default
color   palette-4               196     default
color   palette-5               219     default
color   palette-6               190     default

# status
color   status.header           221     default
color   status.section          81      default
color   stat_staged             213     default
color   stat_unstaged           213     default
color   stat_untracked          213     default

# raw commit header
color   commit                  156     default
color   committer               213     default

# commit message
color   'Signed-off-by'         221     default
color   'Acked-by'              221     default
color   'Tested-by'             221     default
color   'Reviewed-by'           221     default

# tree
color tree.directory 221 default
