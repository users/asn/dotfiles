# Cryptomilk's dotfiles

These are my dotfiles managed using [chezmoi](https://www.chezmoi.io/).

## Neovim

Requirements:

* [gcc]() (for tree-sitter languages)
* [lua51-devel](https://lua.org/) (for luarocks, neorg)
* [ripgrep](https://github.com/BurntSushi/ripgrep) (for telsecope)
* [bfs](https://github.com/tavianator/bfs) (for telescope, nvim-dap)
* [xxd](https://vim.org/) (for hex)

Optional:

* [clangd](https://llvm.org/)
* [bash-language-server](https://github.com/bash-lsp/bash-language-server/)
* [pylsp]
* [ruby-lsp]
* [vale](https://github.com/errata-ai/vale)
* [codespell](https://github.com/codespell-project/codespell/)
* [shellcheck](https://hackage.haskell.org/package/ShellCheck)
* [shfmt](https://github.com/mvdan/sh)
* [stylua](https://github.com/JohnnyMorganz/StyLua)
* [yamllint]
* [prettier]
* [yamlfmt]
* [jq]
