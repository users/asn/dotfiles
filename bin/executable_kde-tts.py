#!/usr/bin/env python3
#
#######################################################################
#
# Copyright (c) 2024      Andreas Schneider <asn@cryptomilk.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################
#
# Requires: python3-speechd
#

from optparse import OptionParser
from speechd import client
import os
import sys


def desc_to_german_time(desc: str):
    if len(desc) == 0:
        return None

    starts_in_str = "Event starts in "
    starts_in_len = len(starts_in_str)
    text = None

    if (desc.startswith(starts_in_str)):
        text = desc[starts_in_len:]

        if text == "1 minute":
            text = "einer Minute"
        else:
            text = text.replace('minutes', 'Minuten')

    return text


def event_calendar(title: str, desc: str):
    text = "Andreas, du hast einen Termin: %s" % (title)

    time = desc_to_german_time(desc)
    if time is not None:
        text = "Andreas, du hast einen Termin in %s: %s" % (time, title)

    return text


def main():
    # Make sure we have a real language set. This is required by speechd
    lang = os.environ["LANG"]
    if lang is None or len(lang) == 0 or lang == 'C':
        os.environ['LANG'] = "en_US.UTF-8"

    parser = OptionParser()

    # id: 2
    parser.add_option(
        "-i",
        "--id",
        dest="id",
        help="The notification id",
        metavar="ID"
    )

    # appname: kalendarac
    parser.add_option(
        "-a",
        "--appname",
        dest="appname",
        help="The notification application name",
        metavar="APPNAME"
    )

    # eventid: alarm
    parser.add_option(
        "-e",
        "--eventid",
        dest="eventid",
        help="The notification event id",
        metavar="EVENTID"
    )

    # appdispname: Reminders
    parser.add_option(
        "-n",
        "--appdispname",
        dest="appdispname",
        help="The notification application display name",
        metavar="APPDISPNAME"
    )

    # title
    parser.add_option(
        "-t",
        "--title",
        dest="title",
        help="The notification title",
        metavar="TITLE"
    )

    # description
    parser.add_option(
        "-d",
        "--description",
        dest="description",
        help="The notification description",
        metavar="DESC"
    )

    (options, args) = parser.parse_args()

    text = None
    if options.appname == 'kalendarac':
        text = event_calendar(options.title, options.description)

    if text is None:
        text = "Es ist ein Fehler aufgetreten."

    c = client.SSIPClient("kde-tts")
    c.set_language("de-de")
    c.speak(text)

    sys.exit(0)


if __name__ == "__main__":
    main()
