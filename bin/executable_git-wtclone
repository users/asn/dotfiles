#!/bin/bash
#
# See https://blog.cryptomilk.org/2023/02/10/sliced-bread-git-worktree-and-bare-repo/
#

if [ $# -lt 1 ]; then
    echo "fatal: You must specify a repository to clone."
    echo
    echo "usage: git wtclone <repo> [<dir>]"

    exit 1
fi

url="${1}"
urldir="$(basename "${url}")"
gitdir="${urldir%.*}"

if [ $# -ge 2 ]; then
    gitdir=${2}
fi

cleanup_tmpdir() {
    popd 2>/dev/null || true
}
trap cleanup_tmpdir SIGINT

cleanup_and_exit() {
    cleanup_tmpdir
    if test "$1" = 0 -o -z "$1" ; then
        exit 0
    else
        exit "${1}"
    fi
}

# Create git directory
mkdir "${gitdir}" || cleanup_and_exit 1
pushd "${gitdir}" || cleanup_and_exit 1

# Clone git repo
git clone --bare "${url}" .bare || cleanup_and_exit 1
echo "gitdir: ./.bare" > .git

sed '/\[remote "origin"\]/ a\\tfetch = +refs/heads/*:refs/remotes/origin/*' -i .bare/config
git fetch || cleanup_and_exit 1

git config --add core.logallrefupdates true

# Checkout main branch
main_branch=$(git remote show origin | awk '/HEAD branch/ {print $NF}')

git branch -f "${main_branch}" "origin/${main_branch}" || cleanup_and_exit 1
git worktree add "${main_branch}" || cleanup_and_exit 1

cleanup_and_exit 0
