export SKIM_DEFAULT_OPTIONS="--height=70% --reverse --ansi --tiebreak=length,index --bind ctrl-d:abort --prompt=\"λ \" --color=dark,fg:15,hl:03,hl+:03,matched_bg:-1,bg+:-1,fg+:06,current_match_bg:-1,cursor:06,info:07,prompt:06"
if [[ -x /usr/bin/rg ]]; then
    export SKIM_DEFAULT_COMMAND="/usr/bin/rg --files --hidden --iglob !\\.git"
fi

function _history_store() {
    if [[ "$SHELL" == *zsh ]]; then
        print -s "$*"
    fi

    if [[ "$SHELL" == *bash ]]; then
        history -s "$*"
    fi
}

alias fvi=_fzf_nvim
_fzf_nvim()
{
    local file
    file=$(/usr/bin/sk -q "$1") && _history_store "nvim $file" && nvim "$file"
}

alias rgg='_fzf_rg_nvim'
_fzf_rg_nvim()
{
    if [[ $# -eq 0 ]]; then
        echo "Usage: rgg PATTERN"
        return 1
    fi
    result=$(/usr/bin/rg --line-number "$@" | /usr/bin/sk --delimiter ':' --preview '/usr/bin/bat --style=numbers --color=always --highlight-line {2} {1}' --preview-window +{2}-/2)
    file=$(echo "${result}" | awk -F ':' '{print $1}')
    line=$(echo "${result}" | awk -F ':' '{print $2}')
    if [[ -n "$file" ]]; then
        _history_store "nvim $file +$line" && nvim $file +$line
    fi
}

alias fgb='_fzf_git_branch'
_fzf_git_branch()
{
    local branch
    branch=$(git for-each-ref --sort=-committerdate --format='%(refname:short)' refs/heads/ | /usr/bin/sk --no-sort --preview 'git log --color=always -100 {}')
    if [[ -n "${branch}" ]]; then
        _history_store git "checkout $branch" && git checkout "$branch"
    fi
}

alias fgbr='_fzf_git_branch_remote'
_fzf_git_branch_remote()
{
    local branch
    branch=$(git for-each-ref --sort=-committerdate --format='%(refname:short)' refs/remotes/ | /usr/bin/sk --no-sort --preview 'git log --color=always -100 {}')
    if [[ -n "${branch}" ]]; then
        _history_store git "checkout $branch" && git checkout "$branch"
    fi
}

alias fcd='_fzf_cd'
_fzf_cd()
{
    local _dir=""
    local _path="${1:-.}"
    echo "PATH: $_path"
    if [[ $# -le 1 ]]; then
        _dir=$(find "${_path}" -type d ! -path "*/.git*" 2>/dev/null | /usr/bin/sk --preview="tree -C -L 1 {}")
    elif [[ $# -eq 2 ]]; then
        _dir=$(find "${_path}" -type d ! -path "*/.git*" 2>/dev/null | /usr/bin/rg -i "$2" | /usr/bin/sk --preview="tree -C -L 1 {}")
    fi
    if [[ -n "${_dir}" ]]; then
        _history_store cd "${_dir}" && cd "${_dir}" || return
    fi
}

alias fzf='sk'
alias ff='sk'
alias ffl='sk --bind "enter:execute(less {})"'
alias ffb='sk --bind "tab:execute(bat --paging=always --pager=less {})"'
