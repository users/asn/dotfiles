# ripgrep: Use a pager if needed
rg() {
    RIPGREP_OPTIONS=(--pretty \
                    --smart-case \
                    --colors line:fg:yellow --colors line:style:bold \
                    --colors path:fg:green --colors path:style:bold \
                    --colors match:fg:red \
                    --colors match:style:nobold)
    if [[ -t 1 ]]; then
        /usr/bin/rg ${RIPGREP_OPTIONS} $@ | \
            /usr/bin/less \
                --LONG-PROMPT \
                --RAW-CONTROL-CHARS \
                --ignore-case \
                --no-init \
                --quit-if-one-screen
    else
        /usr/bin/rg ${RIPGREP_OPTIONS} $@
    fi
}

alias rgni='rg --no-ignore'
