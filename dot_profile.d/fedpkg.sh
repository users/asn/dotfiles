#!/usr/bin/env bash

fedpkg_build() {
    editor_package="vim"
    if [ -n "${EDITOR}" ]; then
        editor_package="$(rpm -qf "$(command -v "${EDITOR}")" | cut -d '-' -f1)"
    fi

    release=${1:-rawhide}
    shift 1

    fedpkg --release="${release}" mockbuild \
        --no-clean-all "$@" \
        -- \
        --enable-plugin=ccache \
        "--additional-package=${editor_package}" \
        --additional-package=ripgrep \
        --additional-package=tmux \
        --additional-package=strace \
        --additional-package=gdb
}
alias fb=fedpkg_build

fb-rh() {
    fedpkg_build rawhide "$@"
}

fb-42() {
    fedpkg_build f42 "$@"
}

fb-41() {
    fedpkg_build f41 "$@"
}

fb-40() {
    fedpkg_build f40 "$@"
}
