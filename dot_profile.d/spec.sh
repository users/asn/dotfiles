function vim_spec () {
    if [ ${#@} -eq 0 ] ; then
        vi $PWD/*.spec
    else
        vi "$@"
    fi
}

