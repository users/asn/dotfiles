#!/bin/bash

# https://nordisch.org/posts/osc-helpers-for-lazy-packagers/
alias ob=osc_build
osc_build() {
    editor_package=vim
    if [ -n "${EDITOR}" ]; then
        editor_package="$(rpm -qf "$(which "${EDITOR}")" | cut -d '-' -f1)"
    fi
    pkg_dir=${1}
    repo=${2}
    shift 2
    osc build -x "${editor_package}" -x ripgrep -k "../rpms/${pkg_dir}" -p "../rpms/${pkg_dir}" "${repo}" x86_64 "$@"
}

ob-tw() {
    osc_build tw openSUSE_Tumbleweed "$@"
}
ob-twf() {
    osc_build tw openSUSE_Factory "$@"
}

ob-15.6() {
    osc_build 15.6 15.6 "$@"
}
ob-15.5() {
    osc_build 15.5 15.5 "$@"
}
ob-15.4() {
    osc_build 15.4 15.4 "$@"
}
ob-15.3() {
    osc_build 15.3 openSUSE_Leap_15.3 "$@"
    ret=$?
    if [[ ${ret} -ne 0 ]]; then
        osc_build 15.3 15.3 "$@"
    fi
}
ob-15.2() {
    osc_build 15.2 openSUSE_Leap_15.2 "$@"
}

ob-f37() {
    osc_build f37 Fedora_Rawhide "$@"
}
ob-f36() {
    osc_build f36 Fedora_36 "$@"
}
ob-f35() {
    osc_build f35 Fedora_35 "$@"
}
