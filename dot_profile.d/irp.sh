irp() {
    name="${1}"
    suffix="${3:+-$3}"
    versioninfo="${2:+-v $2}"
    r="rubygem-${name}${suffix}"

    test -d "${r}" && return 1
    mkdir "${r}"
    pushd "${r}" || return 1
    if [ ! -r "../gem2rpm.yml" ]; then
        popd || true
        rm -rf "${r}"
        echo "../gem2rpm.yml doesn't exist"
        return 1
    fi

    cp ../gem2rpm.yml .
    if [ -n "${suffix}" ]; then
        echo ":version_suffix: '${suffix}'" >>gem2rpm.yml
    fi

    touch "${r}.spec"
    osc vc -m "Initial package"
    gem fetch "${versioninfo}" "${name}"
    gem2rpm --config gem2rpm.yml -o ./*spec ./*gem
    osc add "$(pwd)"
    ob-tw && osc ci -m "Initial package"
    popd || true
}

urp() {
    name="${1}"

    out=$(gem fetch "${name}")
    gem_name=$(echo "${out}" | awk '/Downloaded/ { print $2 }')

    if [ -e gem2rpm.yml ]; then
        cfg="--config=gem2rpm.yml"
    fi
    gem2rpm "${cfg}" --output ./*.spec "${gem_name}.gem"
}
